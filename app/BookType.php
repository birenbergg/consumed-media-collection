<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookType extends Model
{
	protected $table = 'book_types';
	
	public function books() {
		return $this->hasMany('App\Book', 'book_type_id');
	}
}