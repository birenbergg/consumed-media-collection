<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmcType extends Model
{
	public function albums() {
		return $this->hasMany('App\FilterButton', 'type_id');
	}
}