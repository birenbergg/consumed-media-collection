<?php

namespace App\Http\Controllers;

use App\Album;
use App\Artist;
use App\AlbumType;
use App\Country;
use App\Http\Controllers\Controller;

class AlbumsController extends Controller
{
	public function index() {
		$albums = Album::with(['tracks' => function($query) { $query->orderBy('number', 'asc'); }, 'artist', 'type', 'country'])->get();

		foreach ($albums as $album) {
			if ($album->year == null) $album->year = substr($album->date, 0, 4);
			if ($album->month == null) $album->month = substr($album->date, 5, 2);

			if ($album->sort_title == null) $album->sort_title = $album->title;
		}

		return $albums;
    }

	public function indexTypes() {
		return AlbumType::withCount(['albums' => function ($query) {
             $query->where('myself', 0);
        }])->get();
    }

	public function indexCountries() {
		return Country::withCount(['albums' => function ($query) {
             $query->where('myself', 0);
        }])->get();
    }

    public function indexArtists() {
    	$artists = Artist::withCount(['albums' => function ($query) {
             $query->where('myself', 0);
        }])->get();

	 	foreach ($artists as $artist) {
	 		if ($artist->sort_name_en == null && $artist->pseudonym_en != null) $artist->sort_name_en = $artist->pseudonym_en;
	 		if ($artist->sort_name_en == null && $artist->last_name_en != null) $artist->sort_name_en = $artist->last_name_en;
	 		if ($artist->sort_name_en == null && $artist->band_name_en != null) $artist->sort_name_en = $artist->band_name_en;

	 		if ($artist->sort_name_ru == null && $artist->pseudonym_ru != null) $artist->sort_name_ru = $artist->pseudonym_ru;
			if ($artist->sort_name_ru == null && $artist->last_name_ru != null) $artist->sort_name_ru = $artist->last_name_ru;
			if ($artist->sort_name_ru == null && $artist->band_name_ru != null) $artist->sort_name_ru = $artist->band_name_ru;

			if ($artist->sort_name_he == null && $artist->pseudonym_he != null) $artist->sort_name_he = $artist->pseudonym_he;
			if ($artist->sort_name_he == null && $artist->last_name_he != null) $artist->sort_name_he = $artist->last_name_he;
			if ($artist->sort_name_he == null && $artist->band_name_he != null) $artist->sort_name_he = $artist->band_name_he;

			if ($artist->sort_name_uk == null && $artist->pseudonym_uk != null) $artist->sort_name_uk = $artist->pseudonym_uk;
			if ($artist->sort_name_uk == null && $artist->last_name_uk != null) $artist->sort_name_uk = $artist->last_name_uk;
			if ($artist->sort_name_uk == null && $artist->band_name_uk != null) $artist->sort_name_uk = $artist->band_name_uk;
	 	}

    	return $artists;
    }
}