<?php

namespace App\Http\Controllers;

use App\Game;
use App\Http\Controllers\Controller;

class GamesController extends Controller
{
	public function index() {
        $games = Game::where('status', '!=', 1)->get();

		foreach ($games as $game) {
			if ($game->year == null) $game->year = substr($game->date, 0, 4);
			if ($game->month == null) $game->month = substr($game->date, 5, 2);

			if ($game->sort_title == null) $game->sort_title = $game->title;
		}

		return $games;
    }
}