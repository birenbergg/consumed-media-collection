<?php

namespace App\Http\Controllers;

use App\FilterButton;
use App\Http\Controllers\Controller;

class CommonController extends Controller
{
	public function indexButtons() {
		$buttons = FilterButton::all();

		foreach ($buttons as $button) {
			$button->open = false;
			$button->plural_name = $button->plural_name != null ? $button->plural_name : $button->single_name . 's';
		}

		return $buttons;
    }
}