<?php

namespace App\Http\Controllers;

use App\Film;
use App\Http\Controllers\Controller;

class FilmsController extends Controller
{
	public function index() {
        $films = Film::where('status', '=', 2)->get();

		foreach ($films as $film) {
			if ($film->year == null) $film->year = substr($film->date, 0, 4);
			if ($film->month == null) $film->month = substr($film->date, 5, 2);

			if ($film->sort_title == null) $film->sort_title = $film->title;
		}

		return $films;
    }
}