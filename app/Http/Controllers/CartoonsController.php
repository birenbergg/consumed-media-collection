<?php

namespace App\Http\Controllers;

use App\Cartoon;
use App\Http\Controllers\Controller;

class CartoonsController extends Controller
{
	public function index() {
        $cartoons = Cartoon::where('status', '=', 2)->get();

		foreach ($cartoons as $cartoon) {
			if ($cartoon->year == null) $cartoon->year = substr($cartoon->date, 0, 4);
			if ($cartoon->month == null) $cartoon->month = substr($cartoon->date, 5, 2);

			if ($cartoon->sort_title == null) $cartoon->sort_title = $cartoon->title;
		}

		return $cartoons;
    }
}