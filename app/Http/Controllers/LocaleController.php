<?php

namespace App\Http\Controllers;

use Config;
use Cookie;
use Illuminate\Support\Facades\Session;

class LocaleController extends Controller
{
	public function getSetLocale($lang)
	{
		$cookie = Cookie::forever('applocale', $lang);
		Cookie::queue($cookie);

		//if (array_key_exists($lang, Config::get('languages'))) {
            Session::put('applocale', $lang);
        //}
		return redirect()->back();
	}
}