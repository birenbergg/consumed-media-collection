<?php

namespace App\Http\Controllers;

use App\Book;
use App\Author;
use App\Http\Controllers\Controller;

class BooksController extends Controller
{
	public function index() {
		return Book::with('authors', 'series', 'languages', 'type')->where('status', '!=', 1)->where('hierarchy', '=', 0)->get();
    }

    public function indexAuthors() {
        return Author::with('books')->get();
    }
}