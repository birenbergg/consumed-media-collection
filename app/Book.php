<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
	public function authors() {
		return $this->belongsToMany('App\Author', 'author_to_book');
	}

	public function series() {
		return $this->belongsTo('App\BookSeries');
	}

	public function type() {
		return $this->belongsTo('App\BookType');
	}

	public function languages() {
		return $this->belongsToMany('App\Language', 'language_to_book');
	}
}