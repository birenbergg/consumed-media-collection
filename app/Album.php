<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
	public function artist() {
		return $this->belongsTo('App\Artist');
	}

	public function tracks() {
		return $this->belongsToMany('App\Track', 'track_to_album')->withPivot('side_number', 'disc_number', 'number');
	}

	public function type() {
		return $this->belongsTo('App\AlbumType');
	}

	public function country() {
		return $this->belongsTo('App\Country');
	}
}