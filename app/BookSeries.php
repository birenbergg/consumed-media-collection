<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookSeries extends Model
{
	protected $table = 'book_series';
	
	public function books() {
		return $this->hasMany('App\Book', 'series_id');
	}
}