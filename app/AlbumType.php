<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumType extends Model
{
	protected $table = 'album_types';
	
	public function albums() {
		return $this->hasMany('App\Album', 'type_id');
	}
}