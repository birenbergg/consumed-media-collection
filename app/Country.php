<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public function albums() {
		return $this->hasMany('App\Album');
	}
}