<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
	public function albums() {
		return $this->belongsToMany('App\Album', 'track_to_album');
	}
}