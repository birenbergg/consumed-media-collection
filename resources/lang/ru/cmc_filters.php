<?php

return [
	
	'didnt_finish' => 'не закончил',
	'all' => 'все',

	'watched' => 'смотрел',
	'need_to_watch' => 'надо посмотреть',

	'year' => 'год',
		'from' => 'от',
		'to' => 'до',
	'status' => 'статус',
	'series' => 'серия',
	'country' => 'страна',
	'genre' => 'жанр',
	
	'hof' => 'зал славы',

	'released_forms' => 'вышедший вышедших вышедших',

	'from' => 'с',
	'to' => 'по',

	'decade_suffix' => '-е',
];