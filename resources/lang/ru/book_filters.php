<?php

return [
	'forms' => 'книга книги книг',
	'genetive_forms' => 'книгу книги книг',

	'read' => 'читал',
	'read_now' => 'читаю сейчас',
	'need_to_read' => 'надо прочесть',

	'author' => 'автор',
	'original_lang' => 'язык оригинала',
	'lgenre' => 'литературный жанр',
	'book_type' => 'тип книги',
];