<?php

return [
	'header' => 'КПК',
		'albums' => 'Альбомы',
		'books' => 'Книги',
		'cartoons' => 'Мультфильмы',
		'films' => 'Фильмы',
		'games' => 'Игры'
];