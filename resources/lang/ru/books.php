<?php

return [

	'author' => 'Автор',
	'authors' => 'Авторы',
	
	'original_lang' => 'Язык оригинала',
	'original_langs' => 'Языки оригинала',

	'lgenre' => 'Литературный жанр',
	'book_type' => 'Тип книги',

	'full_title' => 'Полное название',
];