<?php

return [

	'loading' => 'Загрузка',
	'close' => 'Закрыть',
	'and' => ' и ',

	'reset_filters' => 'Сбросить фильтры',
	'remove_filter' => 'убрать фильтр',

	'show_filters' => 'А поподробнее?',
	'hide_filters' => 'Скрыть фильтры',
	
	'year_genetive' => 'года',

	'year' => 'Год выпуска',
	'date' => 'Дата выпуска',
	'series' => 'Серия',
	'categories' => 'Прочие категории',
	'uinverse' => 'Вселенная',
	'country' => 'Страна',
	'countries' => 'Страны',
	'genre' => 'Жанр',
	'genres' => 'Жанры',
	'status' => 'Статус',

	'i_listened' => 'Я послушал',
	'i_read' => 'Я читал',
	'i_watched' => 'Я видел',
	'i_played' => 'Я играл в ',

	'by' => 'исполнителя',

	'search_placeholder' => 'Поиск по названию',
];