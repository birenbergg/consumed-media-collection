<?php

return [

	'studio' => 'Студия',
	'studios' => 'Студии',
	
	'length' => 'Формат',
		'feature' => 'полнометражный',
		'short' => 'короткометражный',

	'technique' => 'Тип',
		'trad' => 'рисованный',
		'stomo' => 'кукольный',
		'clay' => 'пластилиновый',
		'comp' => 'компьютерная анимация',

	'cinema' => 'I watched this cartoon in cinema.',
];