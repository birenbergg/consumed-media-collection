<?php

return [
	'forms' => 'game games',
	'genetive_forms' => 'game games',

	'played' => 'played',
	'play_now' => 'play now',
	'need_to_play' => 'need to play',

	'developer' => 'developer',
	'publisher' => 'publisher',
];