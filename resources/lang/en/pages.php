<?php

return [

	'site' => 'Site',

	'albums' => 'Albums',
	'books' => 'Books',
	'cartoons' => 'Cartoons',
	'films' => 'Films',
	'games' => 'Games',
	
	'blog' => 'Blog',
	'rest' => 'Restaurant Reviews',
	'hrenotive' => 'Hrenotive',
	'notebook' => 'Notebook',
	'zhitie' => 'Nix\'s Life',
	
	'about' => 'About Me',
	'contact' => 'Where to Find Me',
	'do' => 'What I Do',
	'outdoors' => 'Where I Was',
	'annoys' => 'I Hate',
	'love' => 'I Love',
	'choice' => 'My Choice',
	'wishlist' => 'Wishlist',
	
	'blacklist' => 'Blacklist',
	'links' => 'Useful Links',
	'web' => 'Websites I Built',
];