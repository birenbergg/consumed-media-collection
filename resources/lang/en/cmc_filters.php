<?php

return [

	'didnt_finish' => 'didn\'t finish',
	'all' => 'all',

	'watched' => 'watched',
	'need_to_watch' => 'need to watch',

	'year' => 'year',
		'from' => 'from',
		'to' => 'to',
	'status' => 'status',
	'series' => 'series',
	'country' => 'country',
	'genre' => 'genre',
	
	'hof' => 'hall of fame',

	'released_forms' => 'released released released',
	'from' => 'from',
	'to' => 'up to',

	'decade_suffix' => 's',
];