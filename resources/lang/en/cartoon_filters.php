<?php

return [
	'forms' => 'cartoon cartoons',
	'genetive_forms' => 'cartoon cartoons',

	'studio' => 'studio',

	'length' => 'length',
		'feature' => 'feature',
		'short' => 'short',

	'technique' => 'technique',
		'trad' => 'traditional',
		'stomo' => 'stop motion (dolls)',
		'clay' => 'stop motion (clay)',
		'comp' => 'computer',
];