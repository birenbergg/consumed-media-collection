<?php

return [

	'loading' => 'Loading',
	'close' => 'Close',
	'and' => ' and ',

	'reset_filters' => 'Reset filters',
	'remove_filter' => 'remove filter',

	'show_filters' => 'Show filters',
	'hide_filters' => 'Hide filters',
	
	'year_genetive' => '',

	'year' => 'Released',
	'date' => 'Released',
	'series' => 'Series',
	'categories' => 'Other categories',
	'uinverse' => 'Universe',
	'country' => 'Country',
	'countries' => 'Countries',
	'genre' => 'Genre',
	'genres' => 'Genres',
	'status' => 'Status',

	'i_listened' => 'I listened to',
	'i_read' => 'I read',
	'i_watched' => 'I saw',
	'i_played' => 'I played ',

	'by' => 'by',

	'search_placeholder' => 'Search by title',
];