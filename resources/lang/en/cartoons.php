<?php

return [
	'studio' => 'Studio',
	'studios' => 'Studios',
	
	'length' => 'Length',
		'feature' => 'Feature',
		'short' => 'Short',

	'technique' => 'Animation technique',
		'trad' => 'Traditional',
		'stomo' => 'Stop motion (dolls)',
		'clay' => 'Stop motion (clay)',
		'comp' => 'Computer',

	'cinema' => 'Я смотрел этот мультфильм в кинотеатре.',
];