<?php

return [

	'author' => 'Author',
	'authors' => 'Authors',
	
	'original_lang' => 'Language',
	'original_langs' => 'Languages',

	'lgenre' => 'Literature form',
	'book_type' => 'Book type',

	'full_title' => 'Full title',
];