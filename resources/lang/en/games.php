<?php

return [

	'developer' => 'Developer',
	'developers' => 'Developers',
	'publisher' => 'Publisher',
	'developer_and_publisher' => 'Developer and publisher',
];