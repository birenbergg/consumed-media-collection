<?php

return [
	'forms' => 'book books',
	'genetive_forms' => 'book books',

	'read' => 'read',
	'read_now' => 'read now',
	'need_to_read' => 'need to read',

	'author' => 'author',
	'original_lang' => 'language',
	'lgenre' => 'literature form',
	'book_type' => 'book type',
];