<?php

return [
	'forms' => 'סרט סרטים m',
	'genetive_forms' => 'סרט סרטים m',

	'studio' => 'אולפן',

	'length' => 'רואך',
		'feature' => 'מלא',
		'short' => 'קצר',

	'technique' => 'טכניקה',
		'trad' => 'קלאסית',
		'stomo' => 'סטופ מושן (בובות)',
		'clay' => 'פלסטלינה (פלסטלינה)',
		'comp' => 'ממוחשבת',
];