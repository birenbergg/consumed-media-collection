<?php

return [

	'loading' => 'טוען',
	'close' => 'סגור',
	'and' => ' ו',

	'reset_filters' => 'אפס פילטרים',
	'remove_filter' => 'הסר פילטר',

	'show_filters' => 'הצג פילטרים',
	'hide_filters' => 'הסתר פילטרים',
	
	'year_genetive' => '',

	'year' => 'יצא לאור',
	'date' => 'יצא לאור',
	'series' => 'סדרה',
	'categories' => 'קטגוריות אחרות',
	'uinverse' => 'קיום',
	'country' => 'מדינה',
	'countries' => 'מדינות',
	'genre' => 'סוגה',
	'genres' => 'סוגות',
	'status' => 'סטטוס',

	'i_listened' => 'שמעתי',
	'i_read' => 'קראתי',
	'i_watched' => 'ראיתי',
	'i_played' => 'שחקתי ב-',

	'by' => 'מאת',

	'search_placeholder' => 'חפש לפי שם',
];