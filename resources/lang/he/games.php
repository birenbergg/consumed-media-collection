<?php

return [

	'developer' => 'פיתוח',
	'developers' => 'פיתוח',
	'publisher' => 'הפצה',
	'developer_and_publisher' => 'פיתוח והפצה',
];