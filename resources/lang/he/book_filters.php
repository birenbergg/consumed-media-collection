<?php

return [
	'forms' => 'ספר ספרים m',
	'genetive_forms' => 'ספר ספרים m',

	'read' => 'קראתי',
	'read_now' => 'קורא עכשיו',
	'need_to_read' => 'צריך לקרוא',

	'author' => 'מאת',
	'original_lang' => 'שפת מקור',
	'lgenre' => 'סוגה ספרותית',
	'book_type' => 'סוג ספר',
];