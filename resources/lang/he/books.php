<?php

return [

	'author' => 'מאת',
	'authors' => 'מאת',
	
	'original_lang' => 'שפת המקור',
	'original_langs' => 'שפות המקור',

	'lgenre' => 'סוגה ספרותית',
	'book_type' => 'סוג ספר',

	'full_title' => 'שם מלא',
];