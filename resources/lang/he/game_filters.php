<?php

return [
	'forms' => 'משחק משחקים m',
	'genetive_forms' => 'משחק משחקים m',

	'played' => 'שיחקתי',
	'play_now' => 'משחק עכשיו',
	'need_to_play' => 'צריך לשחק',

	'developer' => 'פיתוח',
	'publisher' => 'הפצה',
];