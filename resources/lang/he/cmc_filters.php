<?php

return [

	'didnt_finish' => 'לא סיימתי',
	'all' => 'הכל',

	'watched' => 'ראיתי',
	'need_to_watch' => 'צריך לראות',

	'year' => 'שנה',
		'from' => 'מ-',
		'to' => 'עד',
	'status' => 'סטטוס',
	'series' => 'סדרה',
	'country' => 'ארץ',
	'genre' => 'סוגה',
	
	'hof' => 'היכל התהילה',

	'decade_prefix' => 'ה־',
	'decade_suffix' => '־ים',
];