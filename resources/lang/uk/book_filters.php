<?php

return [
	'forms' => 'книга книги книг',
	'genetive_forms' => 'книгу книги книг',

	'read' => 'читав',
	'read_now' => 'читаю зараз',
	'need_to_read' => 'треба прочитати',

	'author' => 'автор',
	'original_lang' => 'мова оригіналу',
	'lgenre' => 'літературний жанр',
	'book_type' => 'тип книги',
];