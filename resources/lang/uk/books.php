<?php

return [

	'author' => 'Автор',
	'authors' => 'Автори',
	
	'original_lang' => 'Мова оригіналу',
	'original_langs' => 'Мови оригіналу',

	'lgenre' => 'Літературний жанр',
	'book_type' => 'Тип книги',

	'full_title' => 'Повна назва',
];