<?php

return [

	'developer' => 'Розробник',
	'developers' => 'Розробники',
	'publisher' => 'Видавець',
	'developer_and_publisher' => 'Розробник і видавець',
];