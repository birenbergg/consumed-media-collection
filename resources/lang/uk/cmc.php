<?php

return [

	'loading' => 'Вантажится',
	'close' => 'Закрити',
	'and' => ' и ',

	'reset_filters' => 'Скинути фільтри',
	'remove_filter' => 'убрать фильтр',

	'show_filters' => 'Показати фильтри',
	'hide_filters' => 'Приховати фильтри',
	
	'year_genetive' => 'року',

	'year' => 'Рік випуску',
	'date' => 'Дата випуску',
	'series' => 'Серія',
	'categories' => 'Інші категориї',
	'uinverse' => 'Всесвіт',
	'country' => 'Країна',
	'countries' => 'Країни',
	'genre' => 'Жанр',
	'genres' => 'Жанри',
	'status' => 'Статус',

	'i_listened' => 'Я послухав',
	'i_read' => 'Я читав',
	'i_watched' => 'Я бачив',
	'i_played' => 'Я грав в ',

	'by' => 'виконавця',

	'search_placeholder' => 'Пошук за назвою',
];