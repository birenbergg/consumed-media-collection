<?php

return [
	
	'didnt_finish' => 'не закінчив',
	'all' => 'усе',

	'watched' => 'дивився',
	'need_to_watch' => 'треба подивитись',

	'year' => 'рік',
		'from' => 'від',
		'to' => 'до',
	'status' => 'статус',
	'series' => 'серія',
	'country' => 'країна',
	'genre' => 'жанр',
	
	'hof' => 'зала слави',

	'decade_suffix' => '-і',
];