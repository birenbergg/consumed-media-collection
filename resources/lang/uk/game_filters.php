<?php

return [
	'forms' => 'гра гри ігор',
	'genetive_forms' => 'гру гри ігор',

	'played' => 'грав',
	'play_now' => 'граю зараз',
	'need_to_play' => 'треба зіграти',

	'developer' => 'розробник',
	'publisher' => 'видавець',
];