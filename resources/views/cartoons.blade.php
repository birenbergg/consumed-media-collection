@extends('layout.master')

@section('title', '{{ trans("pages.cartoons") }}')
@section('cmc_controller', 'cartoonsController')

@section('controllers')
	@parent
	<script src="/js/controllers/cartoonsController.js"></script>
@endsection

@section('filters')
	@parent
	<script src="/js/filters/cartoonsFilters.js"></script>
@endsection

@section('cmc_content')
	<div ng-repeat="c in cartoons | orderBy: order as totalFiltered" class="cmc-thumbnail">
		<div class="image-wrapper-wrapper">
			<div class="image-wrapper">
				<img ng-src="/images/cartoons/@{{ c.url }}.jpg" alt="@{{ c.title_{!! App::getLocale() !!} }}" ng-class="selectedStatus == 0 && c.status == 1 ? 'notyet' : ''" ng-click="showModal(c.id)" onerror="this.onerror=null;this.src='/images/cartoons/nocover_en.jpg';" />
			</div>
		</div>
		<p>@{{ c.title_{!! App::getLocale() !!} }}</p>
	</div>
@endsection