@extends('layout.master')

@section('title', '{{ trans("pages.games") }}')
@section('cmc_controller', 'gamesController')

@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="/css/games.css">
@endsection

@section('controllers')
	@parent
	<script src="/js/controllers/gamesController.js"></script>
@endsection

@section('filters')
	@parent
	<script src="/js/filters/gamesFilters.js"></script>
@endsection

@section('cmc_content')
	<gg-game ng-repeat="game in games | orderBy: order as totalFiltered" ></gg-game>
@endsection