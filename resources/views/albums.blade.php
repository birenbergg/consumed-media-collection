@extends('layout.master')

@section('title', '{{ trans("pages.albums") }}')
@section('cmc_controller', 'albumsController')

@section('styles')
	@parent
	<link rel="stylesheet" type="text/css" href="/css/albums.css">
@endsection

@section('controllers')
	@parent
	<script src="/js/controllers/albumsController.js"></script>
@endsection

@section('filters')
	@parent
	<script src="/js/filters/albumsFilters.js"></script>
@endsection

@section('cmc_content')
	<!-- Album List -->
	<div ng-repeat="a in albums | albumFilterByYears: selectedYears | albumFilterByDecades: selectedDecades | albumFilterByArtists: selectedArtists | albumFilterByTypes: selectedTypes | albumFilterByCategories: selectedCategories | albumFilterByCountries: selectedCountries | filter: {myself: 0} | filter: {title: titleToSearch} | orderBy: order as totalFiltered" class="cmc-thumbnail album-thumbnail">
		<img fallback-src="/images/albums/nocover.jpg" ng-src="/images/albums/@{{ a.artist.url }}/@{{ a.slug }}.jpg" alt="@{{ a.title }}" ng-click="showModal(a.id); $event.stopPropagation();" />
		<p dir="@{{ a.dir }}">
			<div style="text-align:center;">
				@{{ a.title }}
			</div>
			<div style="color:#aaa; font-size:85%; text-align: center;">
				@{{ a.artist | artistDisplayName:'{!! App::getLocale() !!}' }}
			</div>
		</p>
	</div>
	<!-- End of Album List -->

	@include('includes.album_modal')
@endsection