@extends('layout.master')

@section('title', '{{ trans("pages.books") }}')
@section('cmc_controller', 'booksController')

@section('controllers')
	@parent
	<script src="/js/controllers/booksController.js"></script>
@endsection

@section('filters')
	@parent
	<script src="/js/filters/booksFilters.js"></script>
@endsection

@section('cmc_content')
	<!-- Book list -->
	<div ng-repeat="b in books | filter: {title_{!! App::getLocale() !!}: titleToSearch} | orderBy: order as totalFiltered" class="cmc-thumbnail"> 
		<div class="image-wrapper-wrapper">
			<div class="image-wrapper">
				<img ng-src="/images/books/@{{ b.url }}.jpg" ng-class="selectedStatus == 0 && b.status == 1 ? 'notyet' : ''" alt="@{{ b.title_{!! App::getLocale() !!} }}" ng-click="showModal(b.id); $event.stopPropagation();" onError="this.onerror=null;this.src='/images/books/nocover_en.jpg';" />
			</div>
		</div>
		<p dir="@{{ b.dir }}">
			<div style="text-align:center;">
				@{{ b.title_{!! App::getLocale() !!} }}
			</div>
			<div style="color:#aaa; font-size:85%; text-align: center;">
				@{{ b.author.first_name_{!! App::getLocale() !!} }}
			</div>
		</p>
	</div>
	<!-- End of Book List -->

	@include('includes.book_modal')
@endsection