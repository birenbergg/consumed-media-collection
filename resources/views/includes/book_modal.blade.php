<div id="modal" class="modal-wrapper" ng-init="editMode = false">
	<div class="my-modal">

		<!-- Modal content-->
		<div class="my-modal-content" ng-click="$event.stopPropagation();">
			<button type="button" class="my-close" ng-click="hideModal()">&times;</button>

			<div class="my-modal-header">
				<h4 class="my-modal-title">
					@{{ selectedItem.title_{!! App::getLocale() !!} }}
					<div ng-hide="(selectedItem.title_{!! App::getLocale() !!} == selectedItem.original_title)">
						<small class="light" style="font-weight: normal">@{{ selectedItem.original_title }}</small>
					</div>
				</h4>
			</div>
			
			<div class="my-modal-body">
				<div>
					<img fallback-src="/images/books/nocover_en.jpg" ng-src="/images/books/@{{ selectedItem.url }}.jpg" alt="@{{ selectedItem.title }}" />
				</div>

				<div>
					<p>
						<strong ng-if="selectedItem.authors.length == 1">{{ trans('books.author') }}:</strong>
						<strong ng-if="selectedItem.authors.length > 1">{{ trans('books.authors') }}:</strong>

						<span ng-repeat="author in selectedItem.authors">@{{ $first ? '' : $last ? ' ' + '{!! trans('cmc.and') !!}' + ' ' : ', '}}@{{ author | authorDisplayName: '{!! App::getLocale() !!}' }}</span>
					</p>

					<p>
						<strong>{{ trans('cmc.year') }}:</strong>
						@{{ selectedItem.year }}
					</p>

					<p ng-if="selectedItem.type != null">
						<strong>{{ trans('books.book_type') }}:</strong>
						@{{ selectedItem.type.name_{!! App::getLocale() !!} }}
					</p>

					<p ng-if="selectedItem.series != null">
						<strong>{{ trans('cmc.series') }}:</strong>
						@{{ selectedItem.series.title_{!! App::getLocale() !!} }}
					</p>

					<p>
						<strong ng-if="selectedItem.languages.length == 1">{{ trans('books.original_lang') }}:</strong>
						<strong ng-if="selectedItem.languages.length > 1">{{ trans('books.original_langs') }}:</strong>

						<span ng-repeat="language in selectedItem.languages">@{{ $first ? '' : $last ? ' ' + '{!! trans('cmc.and') !!}' + ' ' : ', '}}@{{ language.name_{!! App::getLocale() !!} }}</span>

						<span ng-if="selectedItem.read_in_orig">(читал в оригинале)</span>
					</p>

					<p>
						<strong>{{ trans('cmc.status') }}:</strong>
						@{{ selectedItem.status | statusFormatter }}
					</p>
				</div>
				
			</div>

			<div class="close-modal-mobile pseudo-link" ng-click="hideModal()">
				{{ trans('cmc.close') }}
			</div>
		</div>

	</div>
</div>