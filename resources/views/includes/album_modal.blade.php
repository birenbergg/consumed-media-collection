<div id="modal" class="modal-wrapper" ng-init="editMode = false">
	<div class="my-modal @{{ selectedItem.side3 != null ? 'double-album' : '' }}">

		<!-- Modal content-->
		<div class="my-modal-content" ng-click="$event.stopPropagation();">
			<button type="button" class="my-close" ng-click="hideModal()">&times;</button>

			<div class="my-modal-header">
				<h4 class="my-modal-title" dir="@{{ selectedItem.dir }}">
					@{{ selectedItem.title }}
				</h4>
			</div>
			@if (Auth::check())
			<div id="toggle">
				<span ng-class="{selected: editMode}" ng-click="editMode = !editMode">Редактирование</span>
			</div>
			@endif
			
			<div class="my-modal-body">
				<div ng-hide="editMode">
					<img fallback-src="/images/albums/nocover.jpg" ng-src="/images/albums/@{{ selectedItem.artist.url }}/@{{ selectedItem.slug }}.jpg" alt="@{{ selectedItem.title }}" />
				</div>
				<div ng-hide="editMode">
					<p>
						<strong>{{ trans('albums.artist') }}:</strong>
						<span ng-hide="selectedArtists | contains: selectedItem.artist" class="pseudo-link" ng-click="addFilter('artists', selectedItem.artist)">@{{ selectedItem.artist | artistDisplayName:'{!! App::getLocale() !!}' }}</span>
						<span ng-show="selectedArtists | contains: selectedItem.artist" ng-click="removeFilter('artists', selectedItem)" class="remove-filter" title="{{ trans('cmc.remove_filter') }}">@{{ selectedItem.artist | artistDisplayName:'{!! App::getLocale() !!}' }} &times;</span>
						<small ng-hide="(selectedItem.artist | artistDisplayName:'{!! App::getLocale() !!}') == selectedItem.artist.orig_name">(@{{ selectedItem.artist.orig_name }})</small>
					</p>

					<p>
						<strong ng-hide="selectedItem.date || selectedItem.month > 0">{{ trans('cmc.year') }}:</strong>
						<strong ng-show="selectedItem.date || selectedItem.month > 0">{{ trans('cmc.date') }}:</strong>
						@{{ selectedItem.date | date: 'd' }}
						<span ng-show="selectedItem.date">@{{ (1 * selectedItem.month) | monthGenetive_{!! App::getLocale() !!} }}</span>
						<span ng-show="selectedItem.month && !selectedItem.date">@{{ (1 * selectedItem.month) | month_{!! App::getLocale() !!} }}</span>
						<span ng-hide="yearFrom == selectedItem.year || yearTo == selectedItem.year" class="pseudo-link" ng-click="addFilter('years', selectedItem.year)">@{{ selectedItem.year }}</span>
						<span ng-show="yearFrom == selectedItem.year || yearTo == selectedItem.year" ng-click="removeFilter('years', selectedItem.year)" class="remove-filter" title="{{ trans('cmc.remove_filter') }}">@{{ selectedItem.year }} &times;</span>
						<span ng-show="selectedItem.date || selectedItem.month > 0">{{ trans('cmc.year_genetive') }}</span>
					</p>
					<div>@{{ selectedItem.discs }}</div>
					<div class="tracks-wrapper">
						<div ng-show="selectedItem.tracks != null">
							<strong>{{ trans('albums.track_listing') }}:</strong>
							<ol>
								<li ng-repeat="t in selectedItem.tracks | orderBy: 'number'"
									ng-class="{'side-2-start':
									(
										selectedItem.tracks[$index - 1].pivot.side_number != t.pivot.side_number
										&&
										t.pivot.side_number != 1
									)
									||
									(
										selectedItem.tracks[$index - 1].pivot.disc_number != t.pivot.disc_number
										&&
										t.pivot.dics_number != 1
									)
								}">
									<span style="@{{ t.spoken == 1 ? 'font-style: italic' : '' }}" dir="@{{ t.dir }}">@{{ t.name }}</span>
								</li>
							</ol>
						</div>
						
						<!-- <div ng-show="selectedItem.tracks == null">
							<strong ng-show="selectedItem.side3 == null">{{ trans('albums.track_listing') }}:</strong>
							<strong ng-show="selectedItem.side3 != null">{{ trans('albums.disc') }} 1:</strong>
							<ol>
								<li ng-repeat="t in selectedItem.side1 | orderBy: 'number'">
									<span style="@{{ t.spoken == 1 ? 'font-style: italic' : '' }}" dir="@{{ t.dir }}">@{{ t.name }}</span>
								</li>
								<br />
								<li ng-repeat="t in selectedItem.side2 | orderBy: 'number'">
									<span style="@{{ t.spoken == 1 ? 'font-style: italic' : '' }}" dir="@{{ t.dir }}">@{{ t.name }}</span>
								</li>
							</ol>
						</div>
						
						<div ng-show="selectedItem.side3 != null">
							<strong>{{ trans('albums.disc') }} 2:</strong>
							<ol>
								<li ng-repeat="t in selectedItem.side3 | orderBy: 'number'">
									<span style="@{{ t.spoken == 1 ? 'font-style: italic' : '' }}" dir="@{{ t.dir }}">@{{ t.name }}</span>
								</li>
								<br />
								<li ng-repeat="t in selectedItem.side4 | orderBy: 'number'">
									<span style="@{{ t.spoken == 1 ? 'font-style: italic' : '' }}" dir="@{{ t.dir }}">@{{ t.name }}</span>
								</li>
							</ol>
						</div> -->
					</div>
				</div>
				<div ng-show="editMode" class="edit">
					<div class="edit-langs">
						<span ng-click="editLang = 'en'" ng-class="{selected: editLang == 'en'}">en</span>
						<span ng-click="editLang = 'ru'" ng-class="{selected: editLang == 'ru'}">ru</span>
						<span ng-click="editLang = 'he'" ng-class="{selected: editLang == 'he'}">he</span>
						<span ng-click="editLang = 'uk'" ng-class="{selected: editLang == 'uk'}">uk</span>
					</div>
					<label>Название</label>
					<input ng-model="selectedItem.title" />
					<div class="lang-specific" ng-show="editLang == 'en'">
						<label>Имя исполнителя</label>
						<input ng-value="selectedItem.artist.first_name_en" />
						<label>Фамилия исполнителя</label>
						<input ng-value="selectedItem.artist.last_name_en" />
						<label>Название группы</label>
						<input ng-value="selectedItem.artist.band_name_en" />
						<label>Прочее имя</label>
						<input ng-value="selectedItem.artist.pseudonym_en" />
					</div>
					<div class="lang-specific" ng-show="editLang == 'ru'">
						<label>Имя исполнителя</label>
						<input ng-value="selectedItem.artist.first_name_ru" />
						<label>Фамилия исполнителя</label>
						<input ng-value="selectedItem.artist.last_name_ru" />
						<label>Название группы</label>
						<input ng-value="selectedItem.artist.band_name_ru" />
						<label>Прочее имя</label>
						<input ng-value="selectedItem.artist.pseudonym_ru" />
					</div>
					<div class="lang-specific" ng-show="editLang == 'he'">
						<label>Имя исполнителя</label>
						<input ng-value="selectedItem.artist.first_name_he" />
						<label>Фамилия исполнителя</label>
						<input ng-value="selectedItem.artist.last_name_he" />
						<label>Название группы</label>
						<input ng-value="selectedItem.artist.band_name_he" />
						<label>Прочее имя</label>
						<input ng-value="selectedItem.artist.pseudonym_he" />
					</div>
					<div class="lang-specific" ng-show="editLang == 'uk'">
						<label>Имя исполнителя</label>
						<input ng-value="selectedItem.artist.first_name_uk" />
						<label>Фамилия исполнителя</label>
						<input ng-value="selectedItem.artist.last_name_uk" />
						<label>Название группы</label>
						<input ng-value="selectedItem.artist.band_name_uk" />
						<label>Прочее имя</label>
						<input ng-value="selectedItem.artist.pseudonym_uk" />
					</div>

					<label>Слаг</label>
					<input ng-value="selectedItem.slug" ng-model="selectedItem.slug" />

					<label>Дата</label>
					<input ng-value="selectedItem.date" ng-model="selectedItem.date" type="date" />
					<label>Месяц</label>
					<input ng-value="selectedItem.month" ng-model="selectedItem.month" />
					<label>Год</label>
					<input ng-value="selectedItem.year" ng-model="selectedItem.year" />

					<label>Страна @{{ selectedItem.country_id }}</label>
					<ul>
						<li ng-repeat="country in countries">
							<img src="/images/flags/@{{ country.url }}.svg" style="width:2em; box-shadow: 0 0 1px 1px rgba(0, 0, 0, .5);" ng-class="{selected-country: country.id === selectedItem.country_id}">
							<span>@{{ country.name_ru }} @{{ country.id }}
						</li>
					</ul>

				</div>
			</div>

			<div class="close-modal-mobile pseudo-link" ng-click="hideModal()">
				{{ trans('cmc.close') }}
			</div>
		</div>

	</div>
</div>