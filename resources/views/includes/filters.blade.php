<div id="filters">
	<div style="display: flex;align-items: center;height: 100%;margin-right: 1em;">
		<!-- Filter Buttons -->
		<div ng-repeat="button in filterButtons" style="height: 100%;">

			<!-- Filter Button -->
			<div class="filter-button" ng-class="{open: button.open}" ng-click="selectFilterButton($index);">
				@{{ button.description_{!! App::getLocale() !!} }} <span>&blacktriangledown;</span>
			</div>
			<!-- End of Filter Button -->

			<!-- Filter Dropdown -->
			<div class="filter-list" style="position: absolute;" ng-if="button.open" ng-init="button.collection_sort = 'sort_name_{{ App::getLocale() }}'; reverse = false">
				<ul>
					<li ng-show="button.show_search" class="filter-search">
						<input id="@{{ button.plural_name }}-search" type="search" ng-model="button.search_collection">
					</li>

					<li class="filter-sort" ng-show="button.show_sort && filteredCollection.length > 1">
						<span ng-click="button.collection_sort = button.sort_by + '_{{ App::getLocale() }}'; reverse = !reverse">
							<span class="filter-sort-label">Name</span>
							<span ng-show="button.collection_sort == button.sort_by + '_{{ App::getLocale() }}' && reverse">&blacktriangledown;</span>
							<span ng-show="button.collection_sort == button.sort_by + '_{{ App::getLocale() }}' && !reverse">&blacktriangle;</span>
						</span>
						<span ng-click="button.collection_sort = 'albums_count'; reverse = !reverse">
							<span ng-show="button.collection_sort == 'albums_count' && reverse">&blacktriangledown;</span>
							<span ng-show="button.collection_sort == 'albums_count' && !reverse">&blacktriangle;</span>
							<span class="filter-sort-label">Albums</span>
						</span>
					</li>

					<li ng-repeat="item in $eval(button.plural_name) | filter:button.search_collection | orderBy:button.collection_sort:reverse as filteredCollection" ng-show="!item.hasOwnProperty('albums_count') || item.albums_count > 0" style="direction: @{{ '{!! App::getLocale() !!}' == 'he' ? 'rtl' : 'ltr'; }}">
						<label for="@{{ button.single_name }}@{{ $index }}">
							<div style="display: flex; align-items: center;">
								<img ng-show="button.show_image" src="/images/flags/@{{ item.url }}.svg" style="width:2em; box-shadow: 0 0 1px 1px rgba(0, 0, 0, .5); -webkit-margin-end: .5em">
								<span ng-if="button.prefix_{!! App::getLocale() !!} != null">@{{ button.prefix_{!! App::getLocale() !!} }}</span><span>@{{ item | displayName:button.single_name:'{!! App::getLocale() !!}' }}<span ng-if="button.suffix_{!! App::getLocale() !!} != null">@{{ button.suffix_{!! App::getLocale() !!} }}</span>
								<span ng-if="button.show_items_num" class="light small">(@{{ item.albums_count }})</span></span>
							</div>
							<input id="@{{ button.single_name }}@{{ $index }}" type="checkbox" ng-model="item.selected" ng-click="toggleFilter(button.plural_name, item)" />
						</label>
					</li>
				</ul>
			</div>
			<!-- End of Filter Dropdown -->
		</div>
		<!-- End of Filter Buttons -->
	</div>

	<div id="search" style="flex-grow: 1">
		<input id="title-search" type="search" placeholder="{{ trans('cmc.search_placeholder') }}" style="width: 100%; border: 1px solid #ddd; height: 2em;padding: 1em;" ng-model="titleToSearch" />
	</div>
	
	<div id="reset">
		<span ng-click="resetFilters()">
			{{ trans('cmc.reset_filters') }}
		</span>
	</div>
</div>

<div style="white-space: nowrap;">
	<p id="amount">@{{ totalFiltered.length }} @{{ totalFiltered.length | pluralization_{!! App::getLocale() !!}: '{!! trans('book_filters.genetive_forms') !!}' }}</p>
</div>

<!-- Filter Tags -->
<div id="selected-filters">
	<span ng-repeat="a in selectedArtists" ng-click="removeFilter('artists', a)">@{{ a | artistDisplayName:'{!! App::getLocale() !!}' }} <span>&times;</span></span>
	<span ng-repeat="y in selectedYears" ng-click="removeFilter('years', y)">@{{ y.display }} <span>&times;</span></span>
	<span ng-repeat="d in selectedDecades" ng-click="removeFilter('decades', d)">@{{ d.display }}{{ trans('cmc_filters.decade_suffix') }} <span>&times;</span></span>
	<span ng-repeat="t in selectedTypes" ng-click="removeFilter('types', t)">@{{ t.description }} <span>&times;</span></span>
	<span ng-repeat="c in selectedCountries" ng-click="removeFilter('countries', c)">@{{ c | displayName:'country':'{!! App::getLocale() !!}' }} <span>&times;</span></span>
</div>
<!-- End of Filter Tags -->