<div id="spinner">
	<img ng-src="/images/general/ajax-loader.gif" alt="{{ trans('cmc.loading') }}" />
	<p>{{ trans('cmc.loading') }}...</p>
</div>