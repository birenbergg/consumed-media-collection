<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
	<head>
		<title>cMc</title>
		<meta charset="utf-8" />

		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->

		<link rel="apple-touch-icon-precomposed" href="/images/icons/iphone.png" />
		<link rel="icon" type="image/png" href="/images/icons/favicon.png" />

		<link rel="stylesheet" type="text/css" href="/css/site.css">
		<link rel="stylesheet" type="text/css" href="/css/general-layout.css">
		<link rel="stylesheet" type="text/css" href="/css/footer.css">

		<link rel='stylesheet' media='screen and (max-width: 767px)' href='/css/max-width-767.css' />
		<link rel='stylesheet' media='screen and (max-width: 1024px)' href='/css/max-width-1023.css' />

		<link rel="stylesheet" type="text/css" href="/css/modal-overrides.css">
		<link rel="stylesheet" type="text/css" href="/css/cmc.css">

		@yield('styles')

		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body dir="auto" style="direction: {{ App::getLocale() == 'he' ? 'rtl' : 'ltr' }}">
		<header id="cmc-header">
			@include('layout.header')
		</header>
		<main class="cmc">
			<div id="cmc-wrapper" class="ng-cloak" ng-app="CMC" ng-controller="@yield('cmc_controller')">
				@yield('cmc_header_nav')
				<div ng-show="dataIsLoading">
					@include('includes.spinner')
				</div>
				<div id="filter-wrapper" ng-hide="dataIsLoading">
					@include('includes.filters')
				</div>
				<div id="cmc-container">
					@yield('cmc_content')
				</div>
			</div>
		</main>
		<footer>
			@include('layout.footer')
		</footer>

		<!-- Bootstrap  -->
		<!-- // <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
	
		<!-- AngularJS + components -->
		<script src="/js/angular.min.js"></script>
		<script src="/js/app.js"></script>
		<script src="//code.jquery.com/jquery-2.2.3.min.js"></script>

		<!-- Services -->
		<script src="/js/services/dataService.js"></script>

		<!-- Controllers -->
		<script src="/js/controllers/cmcController.js"></script>
		
		<!-- Directives -->
		<script src="/js/directives/cmc.js"></script>

		<!-- Filters -->
		<script src="/js/filters/cmcFilters.js"></script>
		<script src="/js/filters/languageFilters.js"></script>
	
		@yield('scripts')
		@yield('services')
		@yield('controllers')
		@yield('directives')
		@yield('filters')
	</body>
</html>