<div id="footer-middle" class="footer-column">
	<div id="langs">
		@foreach (Config::get('languages') as $lang => $language)
			<a href="{{ route('setlocale', $lang) }}">{{$language}}</a>
			@if (!$loop->last)
				<span class="middot">&middot;</span>
			@endif
		@endforeach
	</div>
	<div id="copyright">Copyright &copy; 2014&ndash;<?= date('Y'); ?>, GZM Software</div>
</div>