<img src="/images/logo.png" style="width: 300px">
<p style="font-size: 16px; font-weight: bolder; font-family: Futura;">Consumed Media Collection</p>

<nav>
	<ul class="cmc-nav">
		<li class="{{ Route::is('albums') || Route::is('home') ? 'current' : '' }}">
			@if(!Route::is('albums') && !Route::is('home'))
			<a href="/albums">
			@endif
				{{ trans('pages.albums') }}
			@if(!Route::is('albums') && !Route::is('home'))
			</a>
			@endif
		</li>

		<li class="{{ Route::is('books') ? 'current' : '' }}">
			@if(!Route::is('books'))
			<a href="/books">
			@endif
				{{ trans('pages.books') }}
			@if(!Route::is('books'))
			</a>
			@endif
		</li>
		
		<li class="{{ Route::is('cartoons') ? 'current' : '' }}">
			@if(!Route::is('cartoons'))
			<a href="/cartoons">
			@endif
				{{ trans('pages.cartoons') }}
			@if(!Route::is('cartoons'))
			</a>
			@endif
		</li>
		
		<li class="{{ Route::is('films') ? 'current' : '' }}">
			@if(!Route::is('films'))
			<a href="/films">
			@endif
				{{ trans('pages.films') }}
			@if(!Route::is('films'))
			</a>
			@endif
		</li>
		
		<li class="{{ Route::is('games') ? 'current' : '' }}">
			@if(!Route::is('games'))
			<a href="/games">
			@endif
				{{ trans('pages.games') }}
			@if(!Route::is('games'))
			</a>
			@endif
		</li>
	</ul>
</nav>