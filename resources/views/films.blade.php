@extends('layout.master')

@section('title', '{{ trans("pages.films") }}')
@section('cmc_controller', 'filmsController')

@section('controllers')
	@parent
	<script src="/js/controllers/filmsController.js"></script>
@endsection

@section('filters')
	@parent
	<script src="/js/filters/filmsFilters.js"></script>
@endsection

@section('cmc_content')
	<div ng-repeat="f in films | orderBy: order as totalFiltered" class="cmc-thumbnail">
		<div class="image-wrapper-wrapper">
			<div class="image-wrapper">
				<img ng-src="/images/films/@{{ f.url }}.jpg" alt="@{{ f.title_{!! App::getLocale() !!} }}" ng-class="selectedStatus == 0 && f.status == 1 ? 'notyet' : ''" ng-click="showModal(f.id)" onerror="this.onerror=null;this.src='/images/films/nocover_en.jpg';" />
			</div>
		</div>

		<p ng-class="selectedStatus == 0 && f.status == 1 ? 'notyet' : ''">@{{ f.title_{!! App::getLocale() !!} }}</p>
	</div>
@endsection