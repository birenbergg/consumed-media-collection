(function(){

	angular.module("CMC").controller("gamesController", ['$scope', '$controller', 'dataService', function ($scope, $controller, dataService) {
		
		cmcName = 'games';

		$controller('cmcController', {$scope: $scope});

		// // Get developers for filter list
		// dataService.getData(cmcName, 'developers').success(function(data) {
		// 	$scope.developers = data;
		// });

		// // Get publishers for filter list
		// dataService.getData(cmcName, 'publishers').success(function(data) {
		// 	$scope.publishers = data;
		// });

		// // Get genres for filter list
		// dataService.getData(cmcName, 'genres').success(function(data) {
		// 	$scope.game_genres = data;
		// });

		// // Get series for filter list
		// dataService.getData(cmcName, 'series').success(function(data) {
		// 	$scope.game_series = data;
		// });

		// Ordering
		$scope.order = "[year, month, date]";

		// Filters initialization
		$scope.selectedStatus = "4";
		$scope.yearFrom = "";
		$scope.yearTo = "";
		$scope.developerFilterTerm = "";
		$scope.publisherFilterTerm = "";
		$scope.genreFilterTerm = "";
		$scope.seriesFilterTerm = "";
		$scope.categoryFilterTerm = "";
		$scope.universeFilterTerm = "";
		$scope.century = "";
		$scope.decade = "";

		// Autocompletes visibilty
		$scope.developersAutocompleteIsVisible = false;
		$scope.publishersAutocompleteIsVisible = false;
		$scope.genresAutocompleteIsVisible = false;
		$scope.seriesAutocompleteIsVisible = false;
		$scope.categoryAutocompleteIsVisible = false;
		$scope.universeAutocompleteIsVisible = false;
		
		$scope.orderBy = function(order) {
			$scope.order = order;
		}

		// Select search terms from modal pseudo links
		$scope.selectStatus = function(status) {
			$scope.selectedStatus = status;
		}
		$scope.selectDeveloper = function(developer) {
			$scope.developerFilterTerm = developer;
			$scope.developersAutocompleteIsVisible = false;
		}
		$scope.selectPublisher = function(publisher) {
			$scope.publisherFilterTerm = publisher;
			$scope.publishersAutocompleteIsVisible = false;
		}
		$scope.selectGenre = function(genre) {
			$scope.genreFilterTerm = genre;
			$scope.genresAutocompleteIsVisible = false;
		}
		$scope.selectSeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
		}
		$scope.selectCategory = function(category) {
			$scope.categoryFilterTerm = category;
			$scope.categoryAutocompleteIsVisible = false;
		}
		$scope.selectUniverse = function(universe) {
			$scope.universeFilterTerm = universe;
			$scope.universeAutocompleteIsVisible = false;
		}


		// Autocomplete visibility
		$scope.showDevelopersAutocomplete = function() {
			$scope.developersAutocompleteIsVisible = true;
		}
		$scope.showPublishersAutocomplete = function() {
			$scope.publishersAutocompleteIsVisible = true;
		}
		$scope.showGenresAutocomplete = function() {
			$scope.genresAutocompleteIsVisible = true;
		}
		$scope.showSeriesAutocomplete = function() {
			$scope.seriesAutocompleteIsVisible = true;
		}
		$scope.showCategoryAutocomplete = function() {
			$scope.categoryAutocompleteIsVisible = true;
		}
		$scope.showUniverseAutocomplete = function() {
			$scope.universeAutocompleteIsVisible = true;
		}


		// Filter (user input)
		$scope.filterByYear = function(year) {
			$scope.yearFrom = $scope.yearTo = year;
			$scope.hideModal();
		}
		$scope.filterByDeveloper = function(developer) {
			$scope.developerFilterTerm = developer;
			$scope.developersAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByPublisher = function(publisher) {
			$scope.publisherFilterTerm = publisher;
			$scope.publishersAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByGenre = function(genre) {
			$scope.genreFilterTerm = genre;
			$scope.genresAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterBySeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByCategory = function(category) {
			$scope.categoryFilterTerm = category;
			$scope.categoryAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByUniverse = function(universe) {
			$scope.universeFilterTerm = universe;
			$scope.universeAutocompleteIsVisible = false;
			$scope.hideModal();
		}
	}]);
}());