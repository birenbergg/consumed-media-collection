(function(){

	angular.module("CMC").controller("cartoonsController", ['$scope', '$controller', '$filter', 'dataService', function ($scope, $controller, $filter, dataService) {
		
		cmcName = 'cartoons';

		$controller('cmcController', {$scope: $scope});

		// // Get studios for filter list
		// dataService.getData(cmcName, 'studios').success(function(data) {
		// 	$scope.studios = data;
		// });

		// // Get countries for filter list
		// dataService.getData('common', 'countries').success(function(data) {
		// 	$scope.countries = data;
		// });

		// // Get series for filter list
		// dataService.getData(cmcName, 'series').success(function(data) {
		// 	$scope.cartoon_series = data;
		// });

		// Ordering
		$scope.order = "[year, month, date]";

		// Filters initialization
		$scope.selectedStatus = "2";
		$scope.yearFrom = "";
		$scope.yearTo = "";
		$scope.studioFilterTerm = "";
		$scope.countryFilterTerm = "";
		$scope.seriesFilterTerm = "";
		$scope.selectedLength = "all";
		$scope.selectedTechnique = "all";
		$scope.century = "";
		$scope.decade = "";

		// Autocompletes visibilty
		$scope.studiosAutocompleteIsVisible = false;
		$scope.countriesAutocompleteIsVisible = false;
		$scope.seriesAutocompleteIsVisible = false;
		
		$scope.orderBy = function(order) {
			$scope.order = order;
		}

		// Select search terms from modal pseudo links
		$scope.selectStatus = function(status) {
			$scope.selectedStatus = status;
		}
		$scope.selectStudio = function(studio) {
			$scope.studioFilterTerm = studio;
			$scope.studiosAutocompleteIsVisible = false;
		}
		$scope.selectCountry = function(country) {
			$scope.countryFilterTerm = country;
			$scope.countriesAutocompleteIsVisible = false;
		}
		$scope.selectSeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
		}
		$scope.selectLength = function(length) {
			$scope.selectedLength = length;
			$scope.hideModal();
		}
		$scope.selectTechnique = function(technique) {
			$scope.selectedTechnique = technique;
			$scope.hideModal();
		}


		// Autocomplete visibility
		$scope.showStudiosAutocomplete = function() {
			$scope.studiosAutocompleteIsVisible = true;
		}
		$scope.showCountriesAutocomplete = function() {
			$scope.countriesAutocompleteIsVisible = true;
		}
		$scope.showSeriesAutocomplete = function() {
			$scope.seriesAutocompleteIsVisible = true;
		}


		// Filter (user input)
		$scope.filterByYear = function(year) {
			$scope.yearFrom = $scope.yearTo = year;
			$scope.hideModal();
		}
		$scope.filterByStudio = function(studio) {
			$scope.studioFilterTerm = studio;
			$scope.studiosAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByCountry = function(country) {
			$scope.countryFilterTerm = country;
			$scope.countrysAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.cartoonFilterBySeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
			$scope.hideModal();
		}
	}]);
}());