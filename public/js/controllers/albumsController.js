(function(){

	angular.module("CMC").controller("albumsController", ['$scope', '$controller', '$filter', 'dataService', function ($scope, $controller, $filter, dataService) {

		// $scope.filterButtons = [
		// 	{
		// 		description_en: "Artist",
		// 		description_ru: "Исполнитель",
		// 		description_he: "אמן",
		// 		description_uk: "Виконавець",
		// 		open: false,
		// 		showSearch: true,
		// 		showSort: true,
		// 		showAlbumsNum: true,
		// 		showSuffixes: false,
		// 		public: true,
		// 		showImage: false,
		// 		collectionName: "artists",
		// 		collectionItemName: "artist",
		// 		searchCollection: $scope.searchArtists,
		// 		filteredCollection: 'filteredArtists',
		// 		collectionSort: "artistsSort",
		// 		collectionSortName: "sort_name",
		// 		sorting: [
		// 			{
		// 				description: 'Name',
		// 				prop: 'sort_name'
		// 			},
		// 			{
		// 				description: 'Albums',
		// 				prop: 'albums_count'
		// 			},
		// 		]
		// 	},
		// 	{
		// 		description_en: "Year",
		// 		description_ru: "Год",
		// 		description_he: "שנה",
		// 		description_uk: "",
		// 		open: false,
		// 		showSearch: false,
		// 		showSort: false,
		// 		showAlbumsNum: false,
		// 		showSuffixes: false,
		// 		public: true,
		// 		showImage: false,
		// 		collectionName: "years",
		// 		collectionItemName: "year",
		// 		searchCollection: $scope.searchYears,
		// 		filteredCollection: 'filteredYears',
		// 		collectionSort: "yearsSort"
		// 	},
		// 	{
		// 		description_en: "Decade",
		// 		description_ru: "",
		// 		description_he: "",
		// 		description_uk: "",
		// 		open: false,
		// 		showSearch: false,
		// 		showSort: false,
		// 		showAlbumsNum: false,
		// 		showSuffixes: true,
		// 		public: true,
		// 		showImage: false,
		// 		collectionName: "decades",
		// 		collectionItemName: "decade",
		// 		searchCollection: $scope.searchDecades,
		// 		filteredCollection: 'filteredDecades',
		// 		collectionSort: "decadesSort"
		// 	},
		// 	{
		// 		description_en: "Type",
		// 		description_ru: "",
		// 		description_he: "",
		// 		description_uk: "",
		// 		open: false,
		// 		showSearch: false,
		// 		showSort: false,
		// 		showAlbumsNum: true,
		// 		showSuffixes: false,
		// 		public: true,
		// 		showImage: false,
		// 		collectionName: "types",
		// 		collectionItemName: "type",
		// 		searchCollection: $scope.searchTypes,
		// 		filteredCollection: 'filteredTypes',
		// 		collectionSort: "typesSort",
		// 		collectionSortName: "albums_count"
		// 	},
		// 	{
		// 		description_en: "Category",
		// 		description_ru: "",
		// 		description_he: "",
		// 		description_uk: "",
		// 		open: false,
		// 		showSearch: false,
		// 		showSort: false,
		// 		showAlbumsNum: false,
		// 		showSuffixes: false,
		// 		public: false,
		// 		showImage: false,
		// 		collectionName: "categories",
		// 		collectionItemName: "category",
		// 		searchCollection: $scope.searchCategories,
		// 		filteredCollection: 'filteredCategories',
		// 		collectionSort: "categoriesSort"
		// 	},
		// 	{
		// 		description_en: "Country",
		// 		description_ru: "",
		// 		description_he: "",
		// 		description_uk: "",
		// 		open: false,
		// 		showSearch: true,
		// 		showSort: true,
		// 		showAlbumsNum: true,
		// 		showSuffixes: false,
		// 		public: true,
		// 		showImage: true,
		// 		collectionName: "countries",
		// 		collectionItemName: "country",
		// 		searchCollection: $scope.searchCountry,
		// 		filteredCollection: 'filteredCountries',
		// 		collectionSortName: "name",
		// 		collectionSort: "countriesSort"
		// 	}
		// ];

		$scope.selectedArtists = [];
		$scope.selectedYears = [];
		$scope.selectedDecades = [];
		$scope.selectedTypes = [];
		$scope.selectedCategories = [];
		$scope.selectedCountries = [];

		$scope.years = [];
		$scope.decades = [];
		$scope.types = [];
		$scope.categories = [
			{
				'display': 'Neighbourhood',
				'slug': 'shkhuna'
			},
			{	'display': 'Favorites',
				'slug': 'favorites'
			}
		];
		
		$scope.countries = [];

		$scope.myselfFilterTerm = false;

		$scope.selectFilterButton = function(buttonIndex) {
			for (i = 0; i < $scope.filterButtons.length; i++) {
				$scope.filterButtons[i].open = $scope.filterButtons[i].open ? false : i == buttonIndex;
			}
				
			window.setTimeout(function () {
				var field = document.getElementById($scope.filterButtons[buttonIndex].plural_name + '-search');
				if (field != null) field.focus();
			}, 0);
		}

		

		cmcName = 'albums';

		$controller('cmcController', {$scope: $scope});

		//Get buttons
		dataService.getData('common', 'buttons').then(function(result) {
			$scope.filterButtons = result.data;
		});

		//Get artists for filter list
		dataService.getData(cmcName, 'artists').then(function(result) {
			$scope.artists = result.data;
		});

		//Get types for filter list
		dataService.getData(cmcName, 'types').then(function(result) {
			$scope.types = result.data;
		});

		// Ordering
		$scope.order = "[year, month, day]";

		$scope.orderBy = function(order) {
			$scope.order = order;
		}

		$scope.resetFilters = function () {
			$scope.selectedArtists = [];
			$scope.selectedYears = [];
			$scope.selectedDecades = [];
			$scope.selectedTypes = [];
			$scope.selectedCategories = [];
			$scope.selectedCountries = [];

			angular.forEach($scope.artists, function(a) {
				a.selected = false;
			});

			angular.forEach($scope.years, function(y) {
				y.selected = false;
			});

			angular.forEach($scope.decades, function(d) {
				d.selected = false;
			});

			angular.forEach($scope.types, function(t) {
				t.selected = false;
			});

			angular.forEach($scope.categories, function(c) {
				c.selected = false;
			});

			angular.forEach($scope.countries, function(c) {
				c.selected = false;
			});
		}
	}]);
}());