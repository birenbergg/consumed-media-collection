(function(){

	angular.module("CMC").controller("filmsController", ['$scope', '$controller', 'dataService', function ($scope, $controller, dataService) {
		
		cmcName = 'films';

		$controller('cmcController', {$scope: $scope});

		// // Get directors for filter list
		// dataService.getData(cmcName, 'directors').success(function(data) {
		// 	$scope.directors = data;
		// });

		// // Get countries for filter list
		// dataService.getData('common', 'countries').success(function(data) {
		// 	$scope.countries = data;
		// });

		// // Get genres for filter list
		// dataService.getData(cmcName, 'genres').success(function(data) {
		// 	$scope.film_genres = data;
		// });

		// // Get series for filter list
		// dataService.getData(cmcName, 'series').success(function(data) {
		// 	$scope.film_series = data;
		// });

		// Ordering
		$scope.order = "[year, month, date]";

		// Filters initialization
		$scope.selectedStatus = "2";
		$scope.yearFrom = "";
		$scope.yearTo = "";
		$scope.directorFilterTerm = "";
		$scope.countryFilterTerm = "";
		$scope.genreFilterTerm = "";
		$scope.seriesFilterTerm = "";
		$scope.century = "";
		$scope.decade = "";

		// Autocompletes visibilty
		$scope.directorsAutocompleteIsVisible = false;
		$scope.countriesAutocompleteIsVisible = false;
		$scope.genresAutocompleteIsVisible = false;
		$scope.seriesAutocompleteIsVisible = false;
		
		$scope.orderBy = function(order) {
			$scope.order = order;
		}

		// Select search terms from modal pseudo links
		$scope.selectStatus = function(status) {
			$scope.selectedStatus = status;
		}
		$scope.selectDirector = function(director) {
			$scope.directorFilterTerm = director;
			$scope.directorsAutocompleteIsVisible = false;
		}
		$scope.selectCountry = function(country) {
			$scope.countryFilterTerm = country;
			$scope.countriesAutocompleteIsVisible = false;
		}
		$scope.selectGenre = function(genre) {
			$scope.genreFilterTerm = genre;
			$scope.genresAutocompleteIsVisible = false;
		}
		$scope.selectSeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
		}


		// Autocomplete visibility
		$scope.showDirectorsAutocomplete = function() {
			$scope.directorsAutocompleteIsVisible = true;
		}
		$scope.showCountriesAutocomplete = function() {
			$scope.countriesAutocompleteIsVisible = true;
		}
		$scope.showGenresAutocomplete = function() {
			$scope.genresAutocompleteIsVisible = true;
		}
		$scope.showSeriesAutocomplete = function() {
			$scope.seriesAutocompleteIsVisible = true;
		}


		// Filter (user input)
		$scope.filterByYear = function(year) {
			$scope.yearFrom = $scope.yearTo = year;
			$scope.hideModal();
		}
		$scope.filterByDirector = function(director) {
			$scope.directorFilterTerm = director;
			$scope.directorsAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByCountry = function(country) {
			$scope.countryFilterTerm = country;
			$scope.countrysAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filmFilterByGenre = function(genre) {
			$scope.genreFilterTerm = genre;
			$scope.genresAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filmFilterBySeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
			$scope.hideModal();
		}
	}]);
}());