(function(){

	angular.module("CMC").controller("booksController", ['$scope', '$controller', '$filter', 'dataService', function ($scope, $controller, $filter, dataService) {

		cmcName = 'books';

		$controller('cmcController', {$scope: $scope});

		// Get authors for filter list
		// dataService.getData(cmcName, 'authors').success(function(data) {
		// 	$scope.authors = data;
		// });

		// // Get languages for filter list
		// dataService.getData('common', 'languages').success(function(data) {
		// 	$scope.book_languages = data;
		// });

		// // Get genres for filter list
		// dataService.getData(cmcName, 'genres').success(function(data) {
		// 	$scope.book_genres = data;
		// });

		// // Get book types for filter list
		// dataService.getData(cmcName, 'book_types').success(function(data) {
		// 	$scope.book_types = data;
		// });

		// // Get series for filter list
		// dataService.getData(cmcName, 'series').success(function(data) {
		// 	$scope.book_series = data;
		// });
		
		// Ordering
		$scope.order = "year";

		// Filters initialization
		$scope.selectedStatus = "4";
		$scope.authorFilterTerm = "";
		$scope.yearFrom = "";
		$scope.yearTo = "";
		$scope.languageFilterTerm = "";
		$scope.genreFilterTerm = "";
		$scope.typeFilterTerm = "";
		$scope.seriesFilterTerm = "";
		$scope.century = "";
		$scope.decade = "";

		// Autocompletes visibilty
		$scope.authorsAutocompleteIsVisible = false;
		$scope.languagesAutocompleteIsVisible = false;
		$scope.genresAutocompleteIsVisible = false;
		$scope.typesAutocompleteIsVisible = false;
		$scope.seriesAutocompleteIsVisible = false;

		$scope.orderBy = function(order) {
			$scope.order = order;
		}
		$scope.selectStatus = function(status) {
			$scope.selectedStatus = status;
		}
		$scope.selectAuthor = function(author) {
			$scope.authorFilterTerm = author;
			$scope.authorsAutocompleteIsVisible = false;
		}
		$scope.selectLanguage = function(language) {
			$scope.languageFilterTerm = language;
			$scope.languagesAutocompleteIsVisible = false;
		}
		$scope.selectGenre = function(genre) {
			$scope.genreFilterTerm = genre;
			$scope.genresAutocompleteIsVisible = false;
		}
		$scope.selectType = function(type) {
			$scope.typeFilterTerm = type;
			$scope.typesAutocompleteIsVisible = false;
		}
		$scope.selectSeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
		}


		$scope.showAuthorsAutocomplete = function() {
			$scope.authorsAutocompleteIsVisible = true;
		}
		$scope.showLanguagesAutocomplete = function() {
			$scope.languagesAutocompleteIsVisible = true;
		}
		$scope.showGenresAutocomplete = function() {
			$scope.genresAutocompleteIsVisible = true;
		}		
		$scope.showTypesAutocomplete = function() {
			$scope.typesAutocompleteIsVisible = true;
		}
		$scope.showSeriesAutocomplete = function() {
			$scope.seriesAutocompleteIsVisible = true;
		}


		$scope.filterByYear = function(year) {
			$scope.yearFrom = $scope.yearTo = year;
			$scope.hideModal();
		}
		$scope.filterByAuthor = function(author) {
			$scope.authorFilterTerm = author;
			$scope.authorsAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByLanguage = function(language) {
			$scope.languageFilterTerm = language;
			$scope.languagesAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterByGenre = function(genre) {
			$scope.genreFilterTerm = genre;
			$scope.genresAutocompleteIsVisible = false;
			$scope.hideModal();
		}		
		$scope.filterByType = function(type) {
			$scope.typeFilterTerm = type;
			$scope.typesAutocompleteIsVisible = false;
			$scope.hideModal();
		}
		$scope.filterBySeries = function(series) {
			$scope.seriesFilterTerm = series;
			$scope.seriesAutocompleteIsVisible = false;
			$scope.hideModal();
		}
	}]);
}());