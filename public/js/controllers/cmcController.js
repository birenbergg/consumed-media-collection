(function(){

	angular.module("CMC").controller("cmcController", ['$scope', '$filter', 'dataService', function ($scope, $filter, dataService) {

		//******  Page load  ******

		// Initially show the "Loading" animation
		$scope.dataIsLoading = true;
		
		// Get the main data (generic)
		dataService.getData(cmcName, cmcName).then(function(result) {
			$scope[cmcName] = result.data;
			$scope.dataIsLoading = false; // When finished, hide the "Loading" animation

			// Focus on search field
			window.setTimeout(function () {
				document.getElementById('title-search').focus();
			}, 0);

			$scope.collectYears();
			$scope.collectDecades();
			$scope.sortTracks();
		});

		$scope.editMode = false;
		$scope.editLang = 'en';

		$scope.titleToSearch = '';


		//Get countries for filter list
		dataService.getData('common', 'countries').then(function(result) {
			$scope.countries = result.dataService;
		});

		$scope.collectYears = function() {
			angular.forEach($scope.albums, function(album) {
				var yearToAdd = {
						'display': album.year
					};

				if (!$scope.years.find(function(year){return year.display == album.year})) {
					$scope.years.push(yearToAdd);
				}

				$scope.years.sort(function(a, b){return a.display - b.display});
			});
		}

		$scope.collectDecades = function() {
			angular.forEach($scope.years, function(year) {
				var decadeDisplayToAdd = year.display.toString().substr(0, 3) + '0';

				var decadeToAdd = {
						'display': decadeDisplayToAdd
					};

				if (!$scope.decades.find(function(d){return d.display == decadeDisplayToAdd})) {
					$scope.decades.push(decadeToAdd);
				}

				$scope.decades.sort(function(a, b){return a.display - b.display});
				// console.log($scope.decades);
			});
		}

		$scope.sortTracks = function() {
			for (i = 0; i < $scope.albums.length; i++) {
				var album = $scope.albums[i];
				
				for (j = 0; j < $scope.album.tracks.length; j++) {

					var track = album.tracks[j];

					var albumHasSides = track.pivot.side_number != null;
					var isDoubleAlbum = track.pivot.disc_number != null;

					if (!albumHasSides && !isDoubleAlbum) break;

					// if (album['side' + track.pivot.side_number] == null) {
					// 	album['side' + track.pivot.side_number] = [];
					// }

					// if (track.pivot.side_number != null) {
					// 	album['side' + track.pivot.side_number].push(track);
					// }
				}
			}
			// angular.forEach($scope.albums, function(album) {
			// 	angular.forEach(album.tracks, function(track) {
			// 		var albumHasSides = track.pivot.side_number != null;
			// 		var doubleAlbum = track.pivot.disk_number != null;

			// 		if (!albumHasSides && doubleAlbum) break;

			// 		if (album['side' + track.pivot.side_number] == null) {
			// 			album['side' + track.pivot.side_number] = [];
			// 		}

			// 		if (track.pivot.side_number != null) {
			// 			album['side' + track.pivot.side_number].push(track);
			// 		}
			// 	});

			// 	if (album.side1 != null) {
			// 		delete album.tracks;
			// 	} else {
			// 		delete album.side1;
			// 		delete album.side2;
			// 		delete album.side3;
			// 		delete album.side4;
			// 	}
			// 	console.log(album);

			// 	// angular.forEach(album.tracks, function(track) {
			// 	// 	if (album['disc' + track.pivot.disc_number] == null) {
			// 	// 		album['disc' + track.pivot.disc_number] = [];
			// 	// 	}

			// 	// 	if (track.pivot.disc_number != null) {
			// 	// 		album['disc' + track.pivot.disc_number].push(track);
			// 	// 	}
			// 	// });

			// 	// if (album.disc1 != null) {
			// 	// 	delete album.tracks;
			// 	// } else {
			// 	// 	delete album.disc1;
			// 	// 	delete album.disc2;
			// 	// }
			// 	// console.log(album);


			// 	// if (album['discs'] == null) {
			// 	// 	album['discs'] = [];
			// 	// }

			// 	// angular.forEach(album.tracks, function(track) {
			// 	// 	if (album.discs['disc' + track.pivot.disc_number] == null) {
			// 	// 		album.discs['disc' + track.pivot.disc_number] = {'tracks': []};
			// 	// 	}

			// 	// 	if (track.pivot.disc_number != null) {
			// 	// 		album.discs.push(['disc' + track.pivot.disc_number].tracks.push(track));
			// 	// 	}
			// 	// });

			// 	// if (album.discs.disc1 != null) {
			// 	// 	delete album.tracks;
			// 	// } else {
			// 	// 	delete album.discs;
			// 	// }
			// 	// console.log(album);
			// });
		}


		//******  Filter dropdowns  ******

		// When clicked outside, close all filter dropdowns
		// $(document).click(function(event) {
		// 	for (i = 0; i < $scope.filterButtons.length; i++) {
		// 		$scope.filterButtons[i].open = false;
		// 	}
		// });

		// $('.filter-button').click(function(event) {
		//     event.stopPropagation();
		// });



		//****** Filtering ******

		$scope.toggleFilter = function (collection, item) {

			var collectionItem = {};

			if (item.id == undefined) {
				collectionItem = item;
			} else {
				collectionItem = $filter('getById')($scope[collection], item.id);
			}

	        var selectedCollectionItems = 'selected' + $filter('capitalize')(collection);

	        collectionItem.selected = !collectionItem.selected;

	        if(collectionItem.selected) {
	            collectionItem.selected = false;
	            $scope[selectedCollectionItems].splice($scope[selectedCollectionItems].indexOf(collectionItem), 1);
	        } else {
	            collectionItem.selected = true;
	            $scope[selectedCollectionItems].push(collectionItem);
	        }
	    }

		$scope.addFilter = function (collection, item) {
			var collectionItem = {};

			if (item.id == undefined) {
				collectionItem = item;
			} else {
				collectionItem = $filter('getById')($scope[collection], item.id);
			}
			var selectedCollectionItems = 'selected' + $filter('capitalize')(collection);

			collectionItem.selected = true;
			$scope[selectedCollectionItems].push(collectionItem);
		}

		$scope.removeFilter = function (collection, item) {
			var collectionItem = {};

			if (item.id == undefined) {
				collectionItem = item;
			} else {
				collectionItem = $filter('getById')($scope[collection], item.id);
			}
			var selectedCollectionItems = 'selected' + $filter('capitalize')(collection);

			collectionItem.selected = false;
			$scope[selectedCollectionItems].splice($scope[selectedCollectionItems].indexOf(collectionItem), 1);
		}



		//******  Modal  ******

		// When clicked outside, close modal
		$(document).click(function(event) {
			$scope.hideModal();
		});

		// When Escape pressed, close modal
		$(document).keypress(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		// Same as above, Chrome wrokaround
		$(document).keydown(function(event) {
			if (event.keyCode === 27) {// esc
				$scope.hideModal();
			}
		});

		$scope.showModal = function(id) {
			for (i = 0; i < $scope[cmcName].length; i++) {
				if ($scope[cmcName][i].id == id) {
					$scope.selectedItem = $scope[cmcName][i];
				}
			}

			document.getElementById('modal').classList.add('my-modal-open');
		}

		$scope.hideModal = function() {
			document.getElementById('modal').classList.remove('my-modal-open');
		}

		// $scope.showImageModal = function(id) {
		// 	for (i = 0; i < $scope[cmcName].length; i++) {
		// 		if ($scope[cmcName][i].id == id) {
		// 			$scope.selectedItem = $scope[cmcName][i];
		// 		}
		// 	}

		// 	$scope.hideModal();
		// 	document.getElementById('image-modal').classList.add('my-modal-open');
		// 	document.getElementsByTagName('html')[0].classList.add('noscroll');

		// }

		// $scope.hideImageModal = function(id) {
		// 	document.getElementById('image-modal').classList.remove('my-modal-open');
		// 	document.getElementsByTagName('html')[0].classList.remove('noscroll');

		// 	$scope.showModal(id);
		// }

		//$scope.filterMenuIsVisible = false;
	}]);

}());