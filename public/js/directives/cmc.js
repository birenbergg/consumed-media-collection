(function(){

	var app = angular.module("CMC");

	app.directive('ggAlbum', function() {
		return {
			templateUrl: '/js/templates/album.blade.php',
			restrict: 'E'
		}
	});

	app.directive('ggGame', function() {
		return {
			templateUrl: '/js/templates/game.blade.php',
			restrict: 'E'
		}
	});
}());