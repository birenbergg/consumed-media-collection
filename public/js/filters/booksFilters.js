(function(){

	var app = angular.module("CMC");
		
	app.filter('bookStatusFilter', function() {
		return function(items, status) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if (status == 4) {
					if (item.status == 4 || item.status == 3 || item.status == 2) {
						filtered.push(item);
					}
				} else if (status == 3) {
					if (item.status == 3 || item.status == 2) {
						filtered.push(item);
					}
				} else if(item.status == status || status == 0) {
					filtered.push(item);
				}
			});
			return filtered;
		};
	});

	app.filter('yearFromFilter', function() {
		return function(books, year) {
			var filtered = [];

			angular.forEach(books, function(book) {
				if(book.year >= year || year == '')
					filtered.push(book);
			});
			return filtered;
		};
	});

	app.filter('yearToFilter', function() {
		return function(books, year) {
			var filtered = [];

			angular.forEach(books, function(book) {
				if(book.year <= year || year == '')
					filtered.push(book);
			});
			return filtered;
		};
	});


	app.filter('bookFilterByAuthor', function() {
		return function(books, authorName) {
			authorName = authorName.toLowerCase();
			var filtered = [];

			angular.forEach(books, function(book) {
					if (authorName == '') {
						filtered.push(book);
					} else {
						angular.forEach(book.authors, function(author) {
							if(author.name_en.toLowerCase().match(authorName) || author.name_ru.toLowerCase().match(authorName) || author.name_he.toLowerCase().match(authorName) || author.name_uk.toLowerCase().match(authorName))
								filtered.push(book);
						});
					}
			});
			return filtered;
		};
	});

	app.filter('bookFilterByLanguage', function() {
		return function(books, languageName) {
			languageName = languageName.toLowerCase();
			var filtered = [];

			angular.forEach(books, function(book) {
					if (languageName == '') {
						filtered.push(book);
					} else {
						angular.forEach(book.langs, function(language) {
							if(language.name_en.toLowerCase().match(languageName) || language.name_ru.toLowerCase().match(languageName) || language.name_he.toLowerCase().match(languageName) || language.name_uk.toLowerCase().match(languageName))
								filtered.push(book);
						});
					}
			});
			return filtered;
		};
	});

	app.filter('bookFilterByGenre', function() {
		return function(books, genreName) {
			genreName = genreName.toLowerCase();
			var filtered = [];

			angular.forEach(books, function(book) {
				if(book.lgenre_en.toLowerCase().match(genreName) || book.lgenre_ru.toLowerCase().match(genreName) || book.lgenre_he.toLowerCase().match(genreName) || book.lgenre_uk.toLowerCase().match(genreName) || genreName == '')
					filtered.push(book);
			});
			return filtered;
		};
	});

	app.filter('bookFilterByType', function() {
		return function(books, typeName) {
			typeName = typeName.toLowerCase();
			var filtered = [];

			angular.forEach(books, function(book) {
				if(book.type_en.toLowerCase().match(typeName) || book.type_ru.toLowerCase().match(typeName) || book.type_he.toLowerCase().match(typeName) || book.type_uk.toLowerCase().match(typeName) || typeName == '')
					filtered.push(book);
			});
			return filtered;
		};
	});

	app.filter('bookFilterBySeries', function() {
		return function(books, seriesName) {
			seriesName = seriesName.toLowerCase();
			var filtered = [];

			angular.forEach(books, function(book) {
				if(book.series_en.toLowerCase().match(seriesName) || book.series_ru.toLowerCase().match(seriesName) || book.series_he.toLowerCase().match(seriesName) || book.series_uk.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(book);
			});
			return filtered;
		};
	});

	app.filter('bookFavFilter', function() {
		return function(books, checked) {
			var filtered = [];

			angular.forEach(books, function(book) {
				if(checked) {
					if(book.favorite)
						filtered.push(book);
				}
				else
					filtered.push(book);

			});
			return filtered;
		};
	});


	// Filter filters

	app.filter('authorFilter', function() {
		return function(authors, authorName) {
			authorName = authorName.toLowerCase();
			var filtered = [];

			angular.forEach(authors, function(author) {
				if(author.name_en.toLowerCase().match(authorName) || author.name_ru.toLowerCase().match(authorName) || author.name_he.toLowerCase().match(authorName) || author.name_uk.toLowerCase().match(authorName) || authorName == '')
					filtered.push(author);
			});
			return filtered;
		};
	});

	app.filter('languageFilter', function() {
		return function(languages, languageName) {
			languageName = languageName.toLowerCase();
			var filtered = [];

			angular.forEach(languages, function(language) {
				if(language.name_en.toLowerCase().match(languageName) || language.name_ru.toLowerCase().match(languageName) || language.name_he.toLowerCase().match(languageName) || language.name_uk.toLowerCase().match(languageName) || languageName == '')
					filtered.push(language);
			});
			return filtered;
		};
	});

	app.filter('genreFilter', function() {
		return function(genres, genreName) {
			genreName = genreName.toLowerCase();
			var filtered = [];

			angular.forEach(genres, function(genre) {
				if(genre.name_en.toLowerCase().match(genreName) || genre.name_ru.toLowerCase().match(genreName) || genre.name_he.toLowerCase().match(genreName) || genre.name_uk.toLowerCase().match(genreName) || genreName == '')
					filtered.push(genre);
			});
			return filtered;
		};
	});

	app.filter('typeFilter', function() {
		return function(types, typeName) {
			typeName = typeName.toLowerCase();
			var filtered = [];

			angular.forEach(types, function(type) {
				if(type.name_en.toLowerCase().match(typeName) || type.name_ru.toLowerCase().match(typeName) || type.name_he.toLowerCase().match(typeName) || type.name_uk.toLowerCase().match(typeName) || typeName == '')
					filtered.push(type);
			});
			return filtered;
		};
	});

	app.filter('seriesFilter', function() {
		return function(seriesList, seriesName) {
			seriesName =seriesName.toLowerCase();
			var filtered = [];
			
			angular.forEach(seriesList, function(series) {
				if(series.title_en.toLowerCase().match(seriesName) || series.title_ru.toLowerCase().match(seriesName) || series.title_he.toLowerCase().match(seriesName) || series.title_uk.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(series);
			});
			return filtered;
		};
	});

	var authorDisplayName = function (author, lang) {
		if (author != null) {
			var result = '';
			if (author['first_name_' + lang] != null && author['last_name_' + lang] != null) {
				result += author['first_name_' + lang];

				if (author['middle_name_' + lang] != null) {
					result += ' ' + (author['middle_name_' + lang]);
				}

				if (author['nobiliary_particle_' + lang] != null) {
					result += ' ' + (author['nobiliary_particle_' + lang]);
				}

				result += ' ' + author['last_name_' + lang];

			} else if (author['pseudonym_' + lang] != null) {
				return author['pseudonym_' + lang];
			}

			return result;
		}
	}

	app.filter('authorDisplayName', function () {
		return function (author, lang) {
			if (author != null) return authorDisplayName(author, lang);
		}
	});

	app.filter('statusFormatter', function () {
		return function (status) {
			switch(status) {
				case 1: return 'еще не читал';
				case 2: return 'читаю сейчас';
				case 3: return 'читал, но не закончил';
				case 4: return 'прочел';
			}
		}
	});

}());