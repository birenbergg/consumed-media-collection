(function(){

	var app = angular.module("CMC");

	app.filter('albumFilterByArtists', function() {
		return function(albums, selectedArtists) {
			var filtered = [];

			if (selectedArtists.length == 0) return albums;

			angular.forEach(albums, function (album) {
				angular.forEach(selectedArtists, function (selectedArtist) {
					if (album.artist_id == selectedArtist.id) {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
				});
			});

			return filtered;
		}
	});

	app.filter('albumFilterByYears', function() {
		return function(albums, selectedYears) {
			var filtered = [];

			if (selectedYears.length == 0) return albums;

			angular.forEach(albums, function (album) {
				angular.forEach(selectedYears, function (selectedYear) {
					if (album.year == selectedYear.display) {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
				});
			});

			return filtered;
		}
	});

	app.filter('albumFilterByDecades', function() {
		return function(albums, selectedDecades) {
			var filtered = [];

			if (selectedDecades.length == 0) return albums;

			angular.forEach(albums, function (album) {
				angular.forEach(selectedDecades, function (selectedDecade) {
					if (album.year.toString().substr(0,3) == selectedDecade.display.substr(0,3)) {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
				});
			});

			return filtered;
		}
	});

	app.filter('albumFilterByTypes', function() {
		return function(albums, selectedTypes) {
			var filtered = [];

			if (selectedTypes.length == 0) return albums;

			angular.forEach(albums, function (album) {
				angular.forEach(selectedTypes, function (selectedType) {
					if (album.type_id == selectedType.id) {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
				});
			});

			return filtered;
		}
	});

	app.filter('albumFilterByCategories', function() {
		return function(albums, selectedCategories) {
			var filtered = [];

			if (selectedCategories.length == 0) return albums;

			angular.forEach(albums, function (album) {
				angular.forEach(selectedCategories, function (selectedCategory) {
					if (album.favorite && selectedCategory.slug == 'favorites') {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
					if (album.shkhuna && selectedCategory.slug == 'shkhuna') {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
					if (album.myself && selectedCategory.slug == 'myself') {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
				});
			});

			return filtered;
		}
	});

	app.filter('albumFilterByCountries', function() {
		return function(albums, selectedCountries) {
			var filtered = [];

			if (selectedCountries.length == 0) return albums;

			angular.forEach(albums, function (album) {
				angular.forEach(selectedCountries, function (selectedCountry) {
					if (album.country_id == selectedCountry.id) {
						if (filtered.indexOf(album) == -1) {
							filtered.push(album);
						}
					}
				});
			});

			return filtered;
		}
	});

	var artistDisplayName = function (artist, lang) {
		if (artist != null) {
			if (artist['first_name_' + lang] != null && artist['last_name_' + lang] != null) {
				return artist['first_name_' + lang] + ' ' + artist['last_name_' + lang];
			} else if (artist['band_name_' + lang] != null) {
				return artist['band_name_' + lang];
			} else if (artist['pseudonym_' + lang] != null) {
				return artist['pseudonym_' + lang];
			}
		}
	}

	app.filter('artistDisplayName', function () {
		return function (artist, lang) {
			if (artist != null) return artistDisplayName(artist, lang);
		}
	});

	app.filter('displayName', function () {
		return function (item, itemName, lang) {
			switch (itemName) {
				case 'artist':
					return artistDisplayName(item, lang);
				case 'year':
				case 'category':
				case 'decade':
					return item.display;
				case 'type':
					return item.description;
				case 'country':
					return item['name_' + lang];
				default:
					break;
			}
		}
	});
}());