(function(){

	var app = angular.module("CMC");
	
	app.filter('pluralization_en', function() {
		return function(n, forms, return_number) {
			var f = forms.split(' ');
			var amount = return_number ? n + ' ' : '';

			return amount + (n == 1 ? f[0] : f[1]);
		};
	});

	app.filter('pluralization_ru', function() {
		return function(n, forms, return_number) {
			var f = forms.split(' ');
			var amount = return_number ? n + ' ' : '';

			i = 0;
			if (n % 100 >= 5 && n % 100 <= 20) {
				return amount + f[2];
			} else {
				i = n % 10;
				if (i == 1) {
					return amount + f[0];
				} else if (i >= 2 && i <= 4) {
					return amount + f[1];
				} else {
					return amount + f[2];
				}
			}
		};
	});

	app.filter('pluralization_uk', function() {
		return function(n, forms, return_number) {
			var f = forms.split(' ');
			var amount = return_number ? n + ' ' : '';

			i = 0;
			if (n % 100 >= 5 && n % 100 <= 20) {
				return amount + f[2];
			} else {
				i = n % 10;
				if (i == 1) {
					return amount + f[0];
				} else if (i >= 2 && i <= 4) {
					return amount + f[1];
				} else {
					return amount + f[2];
				}
			}
		};
	});

	app.filter('pluralization_he', function() {
	  return function(n, forms, return_number) {
	  	var f = forms.split(' ');
	  	var amount = return_number ? n + ' ' : '';

	  	if (n == 1)
	    	return f[0]+' '+((f[2] == 'm') ? 'אחד' : 'אחת');
	    else
	    	return amount + f[1];
	  };
	});

	app.filter("month_en", function() {
		return function(month) {
			switch (month) {
				case 1: return "January";
				case 2: return "February";
				case 3: return "March";
				case 4: return "April";
				case 5: return "May";
				case 6: return "June";
				case 7: return "July";
				case 8: return "August";
				case 9: return "September";
				case 10: return "October";
				case 11: return "November";
				case 12: return "December";
			};
		};
	});

	app.filter("month_ru", function() {
		return function(month) {
			switch (month) {
				case 1: return "январь";
				case 2: return "февраль";
				case 3: return "март";
				case 4: return "апрель";
				case 5: return "май";
				case 6: return "июнь";
				case 7: return "июль";
				case 8: return "август";
				case 9: return "сентябрь";
				case 10: return "октябрь";
				case 11: return "ноябрь";
				case 12: return "декабрь";
			};
		};
	});

	app.filter("month_uk", function() {
		return function(month) {
			switch (month) {
				case 1: return "січень";
				case 2: return "лютий";
				case 3: return "березень";
				case 4: return "квітень";
				case 5: return "травень";
				case 6: return "червень";
				case 7: return "липень";
				case 8: return "серпень";
				case 9: return "вересень";
				case 10: return "жовтень";
				case 11: return "листопад";
				case 12: return "грудень";
			};
		};
	});

	app.filter("month_he", function() {
		return function(month) {
			switch (month) {
				case 1: return "ינואר";
				case 2: return "פברואר";
				case 3: return "מרץ";
				case 4: return "אפריל";
				case 5: return "מאי";
				case 6: return "יוני";
				case 7: return "יולי";
				case 8: return "אוגוסט";
				case 9: return "ספטמבר";
				case 10: return "אוקטובר";
				case 11: return "נובמבר";
				case 12: return "דצמבר";
			};
		};
	});

	app.filter("monthGenetive_en", function() {
		return function(month) {
			switch (month) {
				case 1: return "January";
				case 2: return "February";
				case 3: return "March";
				case 4: return "April";
				case 5: return "May";
				case 6: return "June";
				case 7: return "July";
				case 8: return "August";
				case 9: return "September";
				case 10: return "October";
				case 11: return "November";
				case 12: return "December";
			};
		};
	});

	app.filter("monthGenetive_ru", function() {
		return function(month) {
			switch (month) {
				case 1: return "января";
				case 2: return "февраля";
				case 3: return "марта";
				case 4: return "апреля";
				case 5: return "мая";
				case 6: return "июня";
				case 7: return "июля";
				case 8: return "августа";
				case 9: return "сентября";
				case 10: return "октября";
				case 11: return "ноября";
				case 12: return "декабря";
			};
		};
	});

	app.filter("monthGenetive_uk", function() {
		return function(month) {
			switch (month) {
				case 1: return "січня";
				case 2: return "лютого";
				case 3: return "березня";
				case 4: return "квітня";
				case 5: return "травня";
				case 6: return "червня";
				case 7: return "липня";
				case 8: return "серпня";
				case 9: return "вересня";
				case 10: return "жовтня";
				case 11: return "листопада";
				case 12: return "грудня";
			};
		};
	});

	app.filter("monthGenetive_he", function() {
		return function(month) {
			switch (month) {
				case 1: return "בינואר";
				case 2: return "בפברואר";
				case 3: return "במרץ";
				case 4: return "באפריל";
				case 5: return "במאי";
				case 6: return "ביוני";
				case 7: return "ביולי";
				case 8: return "באוגוסט";
				case 9: return "בספטמבר";
				case 10: return "באוקטובר";
				case 11: return "בנובמבר";
				case 12: return "בדצמבר";
			};
		};
	});

}());