(function(){

	var cmc = angular.module("CMC");

	cmc.filter('centuryFilter', function() {
		return function(items, century) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if(item.century == century || century == '')
					filtered.push(item);
			});
			return filtered;
		};
	});

	cmc.filter('decadeFilter', function() {
		return function(items, decade) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if(item.decade == decade || decade == '')
					filtered.push(item);
			});
			return filtered;
		};
	});

	cmc.filter('yearFromFilter', function() {
		return function(items, year) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if(item.year >= year || year == '')
					filtered.push(item);
			});
			return filtered;
		};
	});

	cmc.filter('yearToFilter', function() {
		return function(items, year) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if(item.year <= year || year == '')
					filtered.push(item);
			});
			return filtered;
		};
	});

	cmc.filter('getById', function() {
		return function(input, id) {
			for (i = 0; i < input.length; i++) {
				if (input[i].id == id) {
					return input[i];
				}
			}
			return null;
		}
	});

	

	cmc.filter('capitalize', function () {
	    return function (input) {
	        if (input != null) {
	            input = input.toLowerCase();
	            return input.substring(0, 1).toUpperCase() + input.substring(1);
	        }
	    }
	});

	cmc.filter('contains', function() {
	return function (array, needle) {
		return array.indexOf(needle) >= 0;
		};
	});
}());