(function(){

	var app = angular.module("CMC");
	
	app.filter('filmStatusFilter', function() {
		return function(items, status) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if(item.status == status) {
					filtered.push(item);
				} else if (status == 0) {
					filtered.push(item);
				} else if (item.cinema == 1 && status == 3) {
					filtered.push(item);
				}
			});
			return filtered;
		};
	});

	app.filter('yearFromFilter', function() {
		return function(films, year) {
			var filtered = [];

			angular.forEach(films, function(film) {
				if(film.year >= year || year == '')
					filtered.push(film);
			});
			return filtered;
		};
	});

	app.filter('yearToFilter', function() {
		return function(films, year) {
			var filtered = [];

			angular.forEach(films, function(film) {
				if(film.year <= year || year == '')
					filtered.push(film);
			});
			return filtered;
		};
	});


	app.filter('filmFilterByDirector', function() {
		return function(films, directorName) {
			directorName = directorName.toLowerCase();
			var filtered = [];

			angular.forEach(films, function(film) {
					if (directorName == '') {
						filtered.push(film);
					} else {
						angular.forEach(film.directors, function(director) {
							if(director.name_en.toLowerCase().match(directorName) || director.name_ru.toLowerCase().match(directorName) || director.name_he.toLowerCase().match(directorName))
								filtered.push(film);
						});
					}
			});
			return filtered;
		};
	});

	app.filter('filmFilterByCountry', function() {
		return function(films, countryName) {
			countryName = countryName.toLowerCase();
			var filtered = [];

			angular.forEach(films, function(film) {
					if (countryName == '') {
						filtered.push(film);
					} else {
						angular.forEach(film.countries, function(country) {
							if(country.name_en.toLowerCase().match(countryName) || country.name_ru.toLowerCase().match(countryName) || country.name_he.toLowerCase().match(countryName) || country.short_name_he.toLowerCase().match(countryName))
								filtered.push(film);
						});
					}
			});
			return filtered;
		};
	});

	app.filter('filmFilterByGenre', function() {
		return function(films, genreName) {
			genreName = genreName.toLowerCase();
			var filtered = [];

			angular.forEach(films, function(film) {
				if (genreName == '') {
					filtered.push(film);
				} else {
					angular.forEach(film.genres, function(genre) {
						if(genre.name_en.toLowerCase().match(genreName) || genre.name_ru.toLowerCase().match(genreName) || genre.name_he.toLowerCase().match(genreName))
							filtered.push(film);
					});
				}
			});
			return filtered;
		};
	});

	app.filter('filmFilterBySeries', function() {
		return function(films, seriesName) {
			seriesName = seriesName.toLowerCase();
			var filtered = [];

			angular.forEach(films, function(film) {
				if(film.series_en.toLowerCase().match(seriesName) || film.series_ru.toLowerCase().match(seriesName) || film.series_he.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(film);
			});
			return filtered;
		};
	});

	app.filter('filmFavFilter', function() {
		return function(films, checked) {
			var filtered = [];

			angular.forEach(films, function(film) {
				if(checked) {
					if(film.favorite)
						filtered.push(film);
				}
				else
					filtered.push(film);

			});
			return filtered;
		};
	});


	// Filter filters

	app.filter('directorFilter', function() {
		return function(directors, directorName) {
			directorName = directorName.toLowerCase();
			var filtered = [];

			angular.forEach(directors, function(director) {
				if(director.name_en.toLowerCase().match(directorName) || director.name_ru.toLowerCase().match(directorName) || director.name_he.toLowerCase().match(directorName) || directorName == '')
					filtered.push(director);
			});
			return filtered;
		};
	});

	app.filter('countryFilter', function() {
		return function(countries, countryName) {
			countryName = countryName.toLowerCase();
			var filtered = [];

			angular.forEach(countries, function(country) {
				if(country.name_en.toLowerCase().match(countryName) || country.name_ru.toLowerCase().match(countryName) || country.name_he.toLowerCase().match(countryName) || country.short_name_he.toLowerCase().match(countryName) || countryName == '')
					filtered.push(country);
			});
			return filtered;
		};
	});

	app.filter('genreFilter', function() {
		return function(genres, genreName) {
			genreName = genreName.toLowerCase();
			var filtered = [];

			angular.forEach(genres, function(genre) {
				if(genre.name_en.toLowerCase().match(genreName) || genre.name_ru.toLowerCase().match(genreName) || genre.name_he.toLowerCase().match(genreName) || genreName == '')
					filtered.push(genre);
			});
			return filtered;
		};
	});

	app.filter('seriesFilter', function() {
		return function(seriesList, seriesName) {
			seriesName = seriesName.toLowerCase();
			var filtered = [];
			
			angular.forEach(seriesList, function(series) {
				if(series.title_en.toLowerCase().match(seriesName) || series.title_ru.toLowerCase().match(seriesName) || series.title_he.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(series);
			});
			return filtered;
		};
	});

}());