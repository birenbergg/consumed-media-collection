(function(){

	var app = angular.module("CMC");

	app.filter('cartoonStatusFilter', function() {
		return function(items, status) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if(item.status == status) {
					filtered.push(item);
				} else if (status == 0) {
					filtered.push(item);
				} else if (item.cinema == 1 && status == 3) {
					filtered.push(item);
				}
			});
			return filtered;
		};
	});

	app.filter('cartoonFilterByStudio', function() {
		return function(cartoons, studioName) {
			studioName = studioName.toLowerCase();
			var filtered = [];

			angular.forEach(cartoons, function(cartoon) {
					if (studioName == '') {
						filtered.push(cartoon);
					} else {
						angular.forEach(cartoon.studios, function(studio) {
							if(studio.name_en.toLowerCase().match(studioName) || studio.name_ru.toLowerCase().match(studioName) || studio.name_he.toLowerCase().match(studioName))
								filtered.push(cartoon);
						});
					}
			});
			return filtered;
		};
	});

	app.filter('cartoonFilterByCountry', function() {
		return function(cartoons, countryName) {
			countryName = countryName.toLowerCase();
			var filtered = [];

			angular.forEach(cartoons, function(cartoon) {
					if (countryName == '') {
						filtered.push(cartoon);
					} else {
						angular.forEach(cartoon.countries, function(country) {
							if(country.name_en.toLowerCase().match(countryName) || country.name_ru.toLowerCase().match(countryName) || country.name_he.toLowerCase().match(countryName))
								filtered.push(cartoon);
						});
					}
			});
			return filtered;
		};
	});

	app.filter('cartoonFilterBySeries', function() {
		return function(cartoons, seriesName) {
			seriesName = seriesName.toLowerCase();
			var filtered = [];

			angular.forEach(cartoons, function(cartoon) {
				if(cartoon.series_en.toLowerCase().match(seriesName) || cartoon.series_ru.toLowerCase().match(seriesName) || cartoon.series_he.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(cartoon);
			});
			return filtered;
		};
	});

	app.filter('cartoonFilterByLength', function() {
		return function(cartoons, length) {
			var filtered = [];
			
			angular.forEach(cartoons, function(cartoon) {
				if(cartoon.feature == 1 && length == "feature") {
					filtered.push(cartoon);
				} else if (cartoon.feature == 0 && length == "short") {
					filtered.push(cartoon);
				} else if (length == "all") {
					filtered.push(cartoon);
				}
			});
			return filtered;
		};
	});

	app.filter('cartoonFilterByTechnique', function() {
		return function(cartoons, technique) {
			var filtered = [];

			angular.forEach(cartoons, function(cartoon) {
				if (technique == cartoon.technique) {
					filtered.push(cartoon);
				} else if (technique == "all") {
					filtered.push(cartoon);
				}
			});
			return filtered;
		};
	});

	app.filter('cartoonFavFilter', function() {
		return function(cartoons, checked) {
			var filtered = [];

			angular.forEach(cartoons, function(cartoon) {
				if(checked) {
					if(cartoon.favorite)
						filtered.push(cartoon);
				}
				else
					filtered.push(cartoon);

			});
			return filtered;
		};
	});


	// Filter filters

	app.filter('studioFilter', function() {
		return function(studios, studioName) {
			studioName = studioName.toLowerCase();
			var filtered = [];

			angular.forEach(studios, function(studio) {
				if(studio.name_en.toLowerCase().match(studioName) || studio.name_ru.toLowerCase().match(studioName) || studio.name_he.toLowerCase().match(studioName) || studioName == '')
					filtered.push(studio);
			});
			return filtered;
		};
	});

	app.filter('countryFilter', function() {
		return function(countries, countryName) {
			countryName = countryName.toLowerCase();
			var filtered = [];

			angular.forEach(countries, function(country) {
				if(country.name_en.toLowerCase().match(countryName) || country.name_ru.toLowerCase().match(countryName) || country.name_he.toLowerCase().match(countryName) || countryName == '')
					filtered.push(country);
			});
			return filtered;
		};
	});

	app.filter('seriesFilter', function() {
		return function(seriesList, seriesName) {
			seriesName = seriesName.toLowerCase();
			var filtered = [];
			
			angular.forEach(seriesList, function(series) {
				if(series.title_en.toLowerCase().match(seriesName) || series.title_ru.toLowerCase().match(seriesName) || series.title_he.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(series);
			});
			return filtered;
		};
	});

}());