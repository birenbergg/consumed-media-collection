(function(){

	var app = angular.module("CMC");

	app.filter("statusDisplay", function() {
	  return function(status) {
	  	switch (status) {
	  		case "2":
	  			switch	(currentSiteLang) {
	  				case "ru": return "Я в процессе прохождения этой игры";
	  				case "he": return "";
	  				default: return "I play this game now";
	  			}
			case "3":
				switch	(currentSiteLang) {
	  				case "ru": return "Я не закончил прохождение этой игры";
	  				case "he": return "לא סיימתי לעבור את המשחק הזה";
	  				default: return "I didn't finish this game";
	  			}
			default:
	  			return "";
	  	}
	  };
	});

	app.filter('gameStatusFilter', function() {
		return function(items, status) {
			var filtered = [];

			angular.forEach(items, function(item) {
				if (status == 4) {
					if (item.status == 4 || item.status == 3 || item.status == 2) {
						filtered.push(item);
					}
				} else if (status == 3) {
					if (item.status == 3 || item.status == 2) {
						filtered.push(item);
					}
				} else if(item.status == status || status == 0)
					filtered.push(item);
			});
			return filtered;
		};
	});

	app.filter('yearFromFilter', function() {
		return function(games, year) {
			var filtered = [];

			angular.forEach(games, function(game) {
				if(game.year >= year || year == '')
					filtered.push(game);
			});
			return filtered;
		};
	});

	app.filter('yearToFilter', function() {
		return function(games, year) {
			var filtered = [];

			angular.forEach(games, function(game) {
				if(game.year <= year || year == '')
					filtered.push(game);
			});
			return filtered;
		};
	});


	app.filter('gameFilterByDeveloper', function() {
		return function(games, developerName) {
			developerName = developerName.toLowerCase();
			var filtered = [];

			angular.forEach(games, function(game) {
					if (developerName == '') {
						filtered.push(game);
					} else {
						angular.forEach(game.developers, function(developer) {
							if(developer.name_en.toLowerCase().match(developerName) || developer.name_ru.toLowerCase().match(developerName) || developer.name_he.toLowerCase().match(developerName) || developer.name_uk.toLowerCase().match(developerName))
								filtered.push(game);
						});
					}
			});
			return filtered;
		};
	});

	app.filter('gameFilterByPublisher', function() {
		return function(games, publisherName) {
			publisherName = publisherName.toLowerCase();
			var filtered = [];

			angular.forEach(games, function(game) {
				if(game.publisher_en.toLowerCase().match(publisherName) || game.publisher_ru.toLowerCase().match(publisherName) || game.publisher_he.toLowerCase().match(publisherName) || game.publisher_uk.toLowerCase().match(publisherName) || publisherName == '')
					filtered.push(game);
			});
			return filtered;
		};
	});

	app.filter('gameFilterByGenre', function() {
		return function(games, genreName) {
			genreName = genreName.toLowerCase();
			var filtered = [];

			angular.forEach(games, function(game) {
				if (genreName == '') {
					filtered.push(game);
				} else {
					angular.forEach(game.genres, function(genre) {
						if(genre.name_en.toLowerCase().match(genreName) || genre.name_ru.toLowerCase().match(genreName) || genre.name_he.toLowerCase().match(genreName) || genre.name_uk.toLowerCase().match(genreName))
							filtered.push(game);
					});
				}
			});
			return filtered;
		};
	});

	app.filter('gameFilterBySeries', function() {
		return function(games, seriesName) {
			seriesName = seriesName.toLowerCase();
			var filtered = [];

			angular.forEach(games, function(game) {
				if(game.series_en.toLowerCase().match(seriesName) || game.series_ru.toLowerCase().match(seriesName) || game.series_he.toLowerCase().match(seriesName) || game.series_uk.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(game);
			});
			return filtered;
		};
	});

	app.filter('gameFilterByCategory', function() {
		return function(games, categoryName) {
			categoryName = categoryName.toLowerCase();
			var filtered = [];

			angular.forEach(games, function(game) {
				if (categoryName == '') {
					filtered.push(game);
				} else {
					angular.forEach(game.categories, function(category) {
						if(category.title_en.toLowerCase().match(categoryName) || category.title_ru.toLowerCase().match(categoryName) || category.title_he.toLowerCase().match(categoryName) || category.title_uk.toLowerCase().match(categoryName))
							filtered.push(game);
					});
				}
			});
			return filtered;
		};
	});

	app.filter('gameFilterByUniverse', function() {
		return function(games, universeName) {
			universeName = universeName.toLowerCase();
			var filtered = [];

			angular.forEach(games, function(game) {
				if (universeName == '') {
					filtered.push(game);
				} else {
					angular.forEach(game.universes, function(universe) {
						if(universe.title.toLowerCase().match(universeName))
							filtered.push(game);
					});
				}
			});
			return filtered;
		};
	});

	app.filter('gameFavFilter', function() {
		return function(games, checked) {
			var filtered = [];

			angular.forEach(games, function(game) {
				if(checked) {
					if(game.favorite)
						filtered.push(game);
				}
				else
					filtered.push(game);

			});
			return filtered;
		};
	});


	// Filter filters

	app.filter('developerFilter', function() {
		return function(developers, developerName) {
			developerName = developerName.toLowerCase();
			var filtered = [];

			angular.forEach(developers, function(developer) {
				if(developer.name_en.toLowerCase().match(developerName) || developer.name_ru.toLowerCase().match(developerName) || developer.name_he.toLowerCase().match(developerName) || developerName == '')
					filtered.push(developer);
			});
			return filtered;
		};
	});

	app.filter('publisherFilter', function() {
		return function(publishers, publisherName) {
			publisherName = publisherName.toLowerCase();
			var filtered = [];

			angular.forEach(publishers, function(publisher) {
				if(publisher.name_en.toLowerCase().match(publisherName) || publisher.name_ru.toLowerCase().match(publisherName) || publisher.name_he.toLowerCase().match(publisherName) || publisherName == '')
					filtered.push(publisher);
			});
			return filtered;
		};
	});

	app.filter('genreFilter', function() {
		return function(genres, genreName) {
			genreName = genreName.toLowerCase();
			var filtered = [];

			angular.forEach(genres, function(genre) {
				if(genre.name_en.toLowerCase().match(genreName) || genre.name_ru.toLowerCase().match(genreName) || genre.name_he.toLowerCase().match(genreName) || genreName == '')
					filtered.push(genre);
			});
			return filtered;
		};
	});

	app.filter('seriesFilter', function() {
		return function(seriesList, seriesName) {
			seriesName = seriesName.toLowerCase();
			var filtered = [];
			
			angular.forEach(seriesList, function(series) {
				if(series.title_en.toLowerCase().match(seriesName) || series.title_ru.toLowerCase().match(seriesName) || series.title_he.toLowerCase().match(seriesName) || seriesName == '')
					filtered.push(series);
			});
			return filtered;
		};
	});

	app.filter('categoryFilter', function() {
		return function(categoryList, categoryName) {
			categoryName = categoryName.toLowerCase();
			var filtered = [];
			
			angular.forEach(categoryList, function(category) {
				if(category.title_en.toLowerCase().match(categoryName) || category.title_ru.toLowerCase().match(categoryName) || category.title_he.toLowerCase().match(categoryName) || categoryName == '')
					filtered.push(category);
			});
			return filtered;
		};
	});

	app.filter('universeFilter', function() {
		return function(universeList, universeName) {
			universeName = universeName.toLowerCase();
			var filtered = [];
			
			angular.forEach(universeList, function(universe) {
				if(universe.title.toLowerCase().match(universeName) || universeName == '')
					filtered.push(universe);
			});
			return filtered;
		};
	});
}());