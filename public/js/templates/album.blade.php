<div class="cmc-thumbnail album-thumbnail">
	<img ng-src="/images/albums/{{ a.artist_url }}/{{ a.slug }}.jpg" alt="{{ a.title }}" ng-click="showModal(a.id)" />
	<p dir="{{ a.dir }}">
		<div style="text-align:center;">
			{{ a.title }}
		</div>
		<div style="color:#ccc; font-size:85%;">
			{{ a.artist_name_en }}
		</div>
	</p>
</div>