<div class="cmc-thumbnail"> 
	<div class="image-wrapper-wrapper">
		<div class="image-wrapper">
			<img ng-src="/images/books/@{{ b.url }}.jpg" ng-class="selectedStatus == 0 && b.status == 1 ? 'notyet' : ''" alt="@{{ b.title_{!! App::getLocale() !!} }}" ng-click="showModal(b.id)" onError="this.onerror=null;this.src='/images/books/nocover_en.jpg';" />
		</div>
	</div>
	<p ng-class="selectedStatus == 0 && b.status == 1 ? 'notyet' : ''">@{{ b.title_{!! App::getLocale() !!} }}</p>
</div>