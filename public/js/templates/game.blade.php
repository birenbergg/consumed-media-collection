<div class="cmc-thumbnail">
	<div class="image-wrapper-wrapper">
		<div class="image-wrapper">
			<img ng-src="/images/games/{{ game.url }}.jpg" alt="{{ game.title }}" ng-class="{'notyet': selectedStatus == 0 && game.status == 1}" ng-click="showModal(game.id)" onError="this.onerror=null;this.src='/images/games/nocover_en.jpg';" />
		</div>
	</div>
	<p ng-class="selectedStatus == 0 && game.status == 1 ? 'notyet' : ''" dir="{{ game.dir }}">{{ game.title }}</p>
</div>