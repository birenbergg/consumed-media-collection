(function(){

	var app = angular.module("CMC");

	app.service("dataService", function ($http) {

		var getData = function(cmcName, dataName) {
			if (cmcName == dataName) {
				return $http.get("/api/" + dataName);
			} else {
				return $http.get("/api/" + cmcName + "/" + dataName);
			}
		};

		return {
			getData: getData
		};
	});
	
}());