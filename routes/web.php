<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/api/albums', 'AlbumsController@index');
Route::get('/api/albums/artists', 'AlbumsController@indexArtists');
Route::get('/api/albums/types', 'AlbumsController@indexTypes');

Route::get('/api/books', 'BooksController@index');
Route::get('/api/books/authors', 'BooksController@indexAuthors');
Route::get('/api/books/types', 'BooksController@indexTypes');
Route::get('/api/books/genres', 'BooksController@indexGenres');
Route::get('/api/books/series', 'BooksController@indexSeries');

Route::get('/api/cartoons', 'CartoonsController@index');
Route::get('/api/cartoons/series', 'CartoonsController@indexSeries');
Route::get('/api/cartoons/studios', 'CartoonsController@indexStudios');

Route::get('/api/films', 'FilmsController@index');
Route::get('/api/films/series', 'FilmsController@indexSeries');
Route::get('/api/films/genres', 'FilmsController@indexGenres');
Route::get('/api/films/directors', 'FilmsController@indexDirectors');

Route::get('/api/games', 'GamesController@index');
Route::get('/api/games/series', 'GamesController@indexSeries');
Route::get('/api/games/genres', 'GamesController@indexGenres');
Route::get('/api/games/developers', 'GamesController@indexDevelopers');
Route::get('/api/games/publishers', 'GamesController@indexPublishers');

Route::get('/api/common/countries', 'AlbumsController@indexCountries');
Route::get('/api/common/languages', 'CommonController@indexLanguages');

Route::get('/api/common/buttons', 'CommonController@indexButtons');

Route::get('/', function () {
	return view('albums');
})->name('home');

Route::get('/albums', function () {
	return view('albums');
})->name('albums');

Route::get('/books', function () {
	return view('books');
})->name('books');

Route::get('/cartoons', function () {
	return view('cartoons');
})->name('cartoons');

Route::get('/films', function () {
	return view('films');
})->name('films');

Route::get('/games', function () {
	return view('games');
})->name('games');

// Utils

Route::get('/set-locale/{lang}', 'LocaleController@getSetLocale')->name('setlocale');

Route::get('/login',function () {
	return view('login');
});

Route::post('/register', 'UserController@postRegister')->name('register');
Route::post('/signin', 'UserController@postSignIn')->name('signin');
Route::get('/logout', 'UserController@getLogout')->name('logout');